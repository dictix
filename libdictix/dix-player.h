/**
 * Dictix / DixPlayer - dix-player.h
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#ifndef __DIX_PLAYER_H__
#define __DIX_PLAYER_H__

#include <glib.h>
#include <glib-object.h>
#include <gst/gst.h>

G_BEGIN_DECLS

#define DIX_TYPE_PLAYER            (dix_player_get_type ())
#define DIX_PLAYER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), DIX_TYPE_PLAYER, DixPlayer))
#define DIX_PLAYER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), DIX_TYPE_PLAYER, DixPlayerClass))
#define DIX_IS_PLAYER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DIX_TYPE_PLAYER))
#define DIX_IS_PLAYER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), DIX_TYPE_PLAYER))
#define DIX_PLAYER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), DIX_TYPE_PLAYER, DixPlayerClass))

typedef struct _DixPlayer          DixPlayer;
typedef struct _DixPlayerClass     DixPlayerClass;
typedef struct _DixPlayerPrivate   DixPlayerPrivate;

struct _DixPlayer
{
	GstPipeline parent;

	/* < private > */
	DixPlayerPrivate *priv;
};

struct _DixPlayerClass
{
	GstPipelineClass parent_class;

	/* < signals > */
	void (*playback_started) (DixPlayer *player);
	void (*playback_stopped) (DixPlayer *player, gboolean eos);

	void (*pipeline_error) (DixPlayer *player, GError *error);
};

GType        dix_player_get_type         (void) G_GNUC_CONST;
DixPlayer*   dix_player_new              ();

void         dix_player_start_playing    (DixPlayer *player, gchar* uri, GError **error);
gint64       dix_player_query_duration   (DixPlayer *player);
gint64       dix_player_query_position   (DixPlayer *player);
gdouble      dix_player_query_progress   (DixPlayer *player, gint64 *position);
void         dix_player_move             (DixPlayer *player, gdouble location);
void         dix_player_pause            (DixPlayer *player);
void         dix_player_stop             (DixPlayer *player);

void         dix_player_set_volume       (DixPlayer *player, gdouble volume);

G_END_DECLS

#endif /* __DIX_PLAYER_H__ */

