/**
 * Dictix / DixRecorder - dix-recorder.h
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#ifndef __DIX_RECORDER_H__
#define __DIX_RECORDER_H__

#include <glib.h>
#include <glib-object.h>
#include <gst/gst.h>

G_BEGIN_DECLS

#define DIX_TYPE_RECORDER            (dix_recorder_get_type ())
#define DIX_RECORDER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), DIX_TYPE_RECORDER, DixRecorder))
#define DIX_RECORDER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), DIX_TYPE_RECORDER, DixRecorderClass))
#define DIX_IS_RECORDER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DIX_TYPE_RECORDER))
#define DIX_IS_RECORDER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), DIX_TYPE_RECORDER))
#define DIX_RECORDER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), DIX_TYPE_RECORDER, DixRecorderClass))

typedef struct _DixRecorder          DixRecorder;
typedef struct _DixRecorderClass     DixRecorderClass;
typedef struct _DixRecorderPrivate   DixRecorderPrivate;

struct _DixRecorder
{
	GstPipeline parent;

	/* < private > */
	DixRecorderPrivate *priv;
};

struct _DixRecorderClass
{
	GstPipelineClass parent_class;

	/* < signals > */
	void (*record_started) (DixRecorder *recorder);
	void (*record_stopped) (DixRecorder *recorder, gchar* uri);

	void (*pipeline_error) (DixRecorder *recorder, GError *error);
};

GType          dix_recorder_get_type          (void) G_GNUC_CONST;
DixRecorder*   dix_recorder_new               (const gchar* directory);

void           dix_recorder_start_recording   (DixRecorder *recorder, GError **error);
void           dix_recorder_stop_recording    (DixRecorder *recorder);

G_END_DECLS

#endif /* __DIX_RECORDER_H__ */

