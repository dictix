/**
 * Dictix / DixTagReader - dix-tag-reader.c
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <gio/gio.h>
#include <gst/gst.h>
#include <gst/pbutils/pbutils.h>

#include "dix-tag-reader.h"

enum {
	PROP_0,
	PROP_FILE_URI
};

struct _DixTagReaderPrivate
{
	gchar *uri;

	GstDiscoverer *discoverer;

	GstBus *bus;

	gchar* title;
	gchar* codec;
	guint64 bytes;
	gint64 duration;
	gint channels;
	gint sample_rate;
	gint bit_rate;
};

#define DIX_TAG_READER_GET_PRIVATE(obj) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), DIX_TYPE_TAG_READER, DixTagReaderPrivate))

G_DEFINE_TYPE (DixTagReader, dix_tag_reader, GST_TYPE_PIPELINE)

static void    dix_tag_reader_load_tag    (const GstTagList *list, const gchar *tag, gpointer *data);

static void
dix_tag_reader_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
	DixTagReader *reader = (DixTagReader *) object;
	DixTagReaderPrivate *priv = reader->priv;

	switch (prop_id) {
		case PROP_FILE_URI:
			priv->uri = g_strdup (g_value_get_string (value));

			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
dix_tag_reader_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
	DixTagReader *reader = (DixTagReader *) object;
	DixTagReaderPrivate *priv = reader->priv;

	switch (prop_id) {
		case PROP_FILE_URI:
			g_value_set_string (value, priv->uri);

			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
dix_tag_reader_finalize (GObject *object)
{
	DixTagReader *reader = (DixTagReader *) object;
	DixTagReaderPrivate *priv = reader->priv;

	if (priv->discoverer != NULL) {
		g_object_unref (G_OBJECT (priv->discoverer));
	}
	priv->discoverer = NULL;

	g_free (priv->uri);
	priv->uri = NULL;

	if (priv->title != NULL) {
		g_free (priv->title);
	}
	priv->title = NULL;
	if (priv->codec != NULL) {
		g_free (priv->codec);
	}
	priv->codec = NULL;

	G_OBJECT_CLASS (dix_tag_reader_parent_class)->finalize (object);
}

static void
dix_tag_reader_class_init (DixTagReaderClass* klass)
{
	GObjectClass *gobject_class = (GObjectClass *) klass;

	gobject_class->set_property = dix_tag_reader_set_property;
	gobject_class->get_property = dix_tag_reader_get_property;
	gobject_class->finalize = dix_tag_reader_finalize;

	g_object_class_install_property (gobject_class,
	                                 PROP_FILE_URI,
	                                 g_param_spec_string ("file-uri",
	                                "FLAC audio file uri",
	                                "Uri of the flac audio file we're about to parse",
	                                 NULL,
	                                 G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE));

	g_type_class_add_private (gobject_class, sizeof (DixTagReaderPrivate));
}

static void
dix_tag_reader_init (DixTagReader* reader)
{
	DixTagReaderPrivate *priv = DIX_TAG_READER_GET_PRIVATE (reader);
	reader->priv = priv;
	GError *error = NULL;

	gst_object_set_name (GST_OBJECT(reader), "DictixTagReader");

	priv->uri = NULL;

	priv->discoverer = gst_discoverer_new (20 * GST_SECOND, NULL);
	if (priv->discoverer == NULL) {
		g_warning (error->message);
		g_error_free (error);

		return;
	}

	priv->title = NULL;
	priv->codec = NULL;
	priv->bytes = -1;
	priv->duration = -1;
	priv->channels = -1;
	priv->sample_rate = -1;
	priv->bit_rate = -1;
}

DixTagReader*
dix_tag_reader_new (const gchar* uri)
{
	g_return_val_if_fail ((uri != NULL), NULL);
	if (g_str_has_suffix (uri, ".flac") == FALSE) {
		g_warning ("TagReader only support FLAC audio file.\n");

		return NULL;
	}

	return g_object_new (DIX_TYPE_TAG_READER, "file-uri", uri, NULL);
}

void
dix_tag_reader_load (DixTagReader *reader)
{
	g_return_if_fail (DIX_IS_TAG_READER (reader));
	DixTagReaderPrivate *priv = reader->priv;
	GError *error = NULL;
/*	GFile *file = NULL;*/
/*	GstDiscovererInfo *info = NULL;*/
/*	GstDiscovererStreamInfo *stream_info = NULL;*/
/*	const GstTagList *tags;*/
/*	GList *streams = NULL;*/
/*	GstClockTime length = 0;*/
	GstElement *source = NULL;
	GstElement *decoder = NULL;
	GstElement *sink = NULL;
	GstMessage *message;
	GstFormat format;
	GFile *file = NULL;
	GFileInfo *info = NULL;
	GstPad *pad = NULL;
	GstCaps *caps = NULL;
	GstStructure *structure = NULL;
	gint64 length = 0;
	gint state;

	if (G_UNLIKELY (priv->uri == NULL)) {
		g_warning ("No file specified...\n");

		return;
	}

/*	file = g_file_new_for_uri (priv->uri);*/
/*	if (file != NULL) {*/
/*		GFileInfo *info = NULL;*/

/*		info = g_file_query_info (file,*/
/*		                          G_FILE_ATTRIBUTE_STANDARD_SIZE,*/
/*		                          G_FILE_QUERY_INFO_NONE,*/
/*		                          NULL,*/
/*		                          NULL);*/

/*		priv->bytes = (guint64) g_file_info_get_size (info);*/
/*	}*/

/*	if (G_LIKELY (priv->discoverer != NULL)) {*/
/*		info = gst_discoverer_discover_uri (priv->discoverer,*/
/*		                                    priv->uri,*/
/*		                                    &error);*/
/*		if (error != NULL) {*/
/*			g_warning (error->message);*/
/*			g_error_free (error);*/

/*			return;*/
/*		}*/

/*		tags = gst_discoverer_info_get_tags (info);*/
/*		if (tags != NULL) {*/
/*			gst_tag_list_foreach (tags,(GstTagForeachFunc) dix_tag_reader_load_tag, reader);*/
/*		}*/
/*	}*/

/*		priv->duration = gst_discoverer_info_get_duration (info);*/

/*		stream_info = gst_discoverer_info_get_stream_info (info);*/
/*		if (stream_info != NULL) {*/


/*			gst_discoverer_stream_info_unref (stream_info);*/
/*		}*/

/*		streams = gst_discoverer_info_get_audio_streams (info);*/
/*		if (streams != NULL) {*/
/*			priv->channels = gst_discoverer_audio_info_get_channels (streams->data);*/
/*			priv->sample_rate = gst_discoverer_audio_info_get_sample_rate (streams->data);*/
/*			priv->bit_rate = gst_discoverer_audio_info_get_bitrate (streams->data);*/

/*			gst_discoverer_stream_info_list_free (streams);*/
/*		}*/

	if (G_LIKELY (priv->bus == NULL)) {
		source = gst_element_factory_make ("giosrc", "DictixTagReaderSource");
		if (source == NULL) {
			return;
		}
		g_object_set (G_OBJECT (source), "location", priv->uri, NULL);
		gst_bin_add (GST_BIN (reader), source);

		decoder = gst_element_factory_make ("flacdec", "DictixTagReaderDecoder");
		if (decoder == NULL) {
			gst_object_unref (source);

			return;
		}
		gst_bin_add (GST_BIN (reader), decoder);
		gst_element_link (source, decoder);

		sink = gst_element_factory_make ("fakesink", "DictixTagReaderSink");
		if (sink == NULL) {
			gst_object_unref (source);
			gst_object_unref (decoder);

			return;
		}
		gst_bin_add (GST_BIN (reader), sink);
		gst_element_link (decoder, sink);

		priv->bus = gst_element_get_bus (GST_ELEMENT (reader));
	}

	/* Go to GST_STATE_PAUSE: decoder will read metadata */
	gst_element_set_state (GST_ELEMENT(reader), GST_STATE_PAUSED);

	while (TRUE) {
		GstTagList *tags = NULL;

		message = gst_bus_timed_pop_filtered (priv->bus,
		                                      GST_CLOCK_TIME_NONE,
		                                      GST_MESSAGE_TAG | GST_MESSAGE_ERROR | GST_MESSAGE_ASYNC_DONE);

		if (GST_MESSAGE_TYPE (message) != GST_MESSAGE_TAG) { 
			/* We've found an error or async is done */
			break;
		}

		gst_message_parse_tag (message, &tags);
		gst_tag_list_foreach (tags,(GstTagForeachFunc) dix_tag_reader_load_tag, reader);

		gst_tag_list_free (tags);
		gst_message_unref (message);
	}

	if (GST_MESSAGE_TYPE (message) == GST_MESSAGE_ERROR) {
		gst_message_parse_error (message, &error, NULL);
		if (error != NULL) {
			g_warning (error->message);

			return;
		}
	}

	state = gst_element_get_state (GST_ELEMENT (reader), NULL, NULL, GST_CLOCK_TIME_NONE);
	if (state != GST_STATE_CHANGE_SUCCESS) {
		g_warning ("Failed going to PAUSE state while metadata reading...");

		return;
	}

	gst_message_unref (message);

	/* At this point we are shure to be in GST_STATE_PAUSE state */
	format = GST_FORMAT_TIME;
	if (gst_element_query_duration (sink, &format, &length) == TRUE) {
		if (format == GST_FORMAT_TIME) {
			priv->duration = length;
		}
	}

	g_object_get (G_OBJECT (source), "file", &file, NULL);
	if (file != NULL) {
		info = g_file_query_info (file,
		                          G_FILE_ATTRIBUTE_STANDARD_SIZE,
		                          G_FILE_QUERY_INFO_NONE,
		                          NULL,
		                          NULL);

		priv->bytes = (guint64) g_file_info_get_size (info);
	}

	pad = gst_element_get_static_pad (sink, "sink");
	if (pad != NULL) {
		caps = gst_pad_get_negotiated_caps (pad);
		if (caps != NULL) {
			structure = gst_caps_get_structure (caps, 0);

			gst_structure_get_int (structure, "channels", &priv->channels);
			gst_structure_get_int (structure, "rate", &priv->sample_rate);
		}
	}

	gst_element_set_state (GST_ELEMENT(reader), GST_STATE_NULL);
}

gchar*
dix_tag_reader_get_title (DixTagReader *reader)
{
	g_return_val_if_fail (DIX_IS_TAG_READER (reader), NULL);
	DixTagReaderPrivate *priv = reader->priv;

	return g_strdup (priv->title);
}

gchar*
dix_tag_reader_get_codec (DixTagReader *reader)
{
	g_return_val_if_fail (DIX_IS_TAG_READER (reader), NULL);
	DixTagReaderPrivate *priv = reader->priv;

	return g_strdup (priv->codec);
}

guint64
dix_tag_reader_get_size (DixTagReader *reader)
{
	g_return_val_if_fail (DIX_IS_TAG_READER (reader), 0);
	DixTagReaderPrivate *priv = reader->priv;

	return (gint64) priv->bytes;
}

gint64
dix_tag_reader_get_duration (DixTagReader *reader)
{
	g_return_val_if_fail (DIX_IS_TAG_READER (reader), -1);
	DixTagReaderPrivate *priv = reader->priv;

	/* GST_FORMAT_TIME is nanoseconds... */
	return priv->duration / GST_SECOND;
}

gint
dix_tag_reader_get_channels (DixTagReader *reader)
{
	g_return_val_if_fail (DIX_IS_TAG_READER (reader), -1);
	DixTagReaderPrivate *priv = reader->priv;

	return priv->channels;
}

gint
dix_tag_reader_get_sample_rate (DixTagReader *reader)
{
	g_return_val_if_fail (DIX_IS_TAG_READER (reader), -1);
	DixTagReaderPrivate *priv = reader->priv;

	return priv->sample_rate;
}

gint
dix_tag_reader_get_bit_rate (DixTagReader *reader)
{
	g_return_val_if_fail (DIX_IS_TAG_READER (reader), -1);
	DixTagReaderPrivate *priv = reader->priv;

	return priv->bit_rate;
}

static void
dix_tag_reader_load_tag (const GstTagList *list, const gchar *tag, gpointer *data)
{
	DixTagReader *reader = (DixTagReader *) data;
	DixTagReaderPrivate *priv = reader->priv;
	const GValue *value = NULL;
	gint size = 0;

	size = gst_tag_list_get_tag_size (list, tag);
	if (size < 1) {
		return;
	}

	value = gst_tag_list_get_value_index (list, tag, 0);

	if (! g_strcmp0 (tag, GST_TAG_TITLE)) {
		priv->title = g_strdup (g_value_get_string (value));
	}
	if (! g_strcmp0 (tag, GST_TAG_AUDIO_CODEC)) {
		priv->codec = g_strdup (g_value_get_string (value));
	}
	if (! g_strcmp0 (tag, GST_TAG_BITRATE)) {
		priv->bit_rate = (gint) g_value_get_uint (value);
	}
	if (! g_strcmp0 (tag, GST_TAG_NOMINAL_BITRATE)) {
		priv->bit_rate = (gint) g_value_get_uint (value);
	}
	if (! g_strcmp0 (tag, GST_TAG_MAXIMUM_BITRATE)) {
		priv->bit_rate = (gint) g_value_get_uint (value);
	}
}

