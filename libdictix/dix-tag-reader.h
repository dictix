/**
 * Dictix / DixTagReader - dix-tag-reader.h
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#ifndef __DIX_TAG_READER_H__
#define __DIX_TAG_READER_H__

#include <glib.h>
#include <glib-object.h>
#include <gst/gst.h>

G_BEGIN_DECLS

#define DIX_TYPE_TAG_READER            (dix_tag_reader_get_type ())
#define DIX_TAG_READER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), DIX_TYPE_TAG_READER, DixTagReader))
#define DIX_TAG_READER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), DIX_TYPE_TAG_READER, DixTagReaderClass))
#define DIX_IS_TAG_READER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DIX_TYPE_TAG_READER))
#define DIX_IS_TAG_READER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), DIX_TYPE_TAG_READER))
#define DIX_TAG_READER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), DIX_TYPE_TAG_READER, DixTagReaderClass))

typedef struct _DixTagReader          DixTagReader;
typedef struct _DixTagReaderClass     DixTagReaderClass;
typedef struct _DixTagReaderPrivate   DixTagReaderPrivate;

struct _DixTagReader
{
	GstPipeline parent;

	/* < private > */
	DixTagReaderPrivate *priv;
};

struct _DixTagReaderClass
{
	GstPipelineClass parent_class;

	/* < signals > */
};

GType           dix_tag_reader_get_type          (void) G_GNUC_CONST;
DixTagReader*   dix_tag_reader_new               (const gchar* uri);

void            dix_tag_reader_load              (DixTagReader *reader);

gchar*          dix_tag_reader_get_title         (DixTagReader *reader);
gchar*          dix_tag_reader_get_codec         (DixTagReader *reader);
guint64         dix_tag_reader_get_size          (DixTagReader *reader);
gint64          dix_tag_reader_get_duration      (DixTagReader *reader);
gint            dix_tag_reader_get_channels      (DixTagReader *reader);
gint            dix_tag_reader_get_sample_rate   (DixTagReader *reader);
gint            dix_tag_reader_get_bit_rate      (DixTagReader *reader);

G_END_DECLS

#endif /* __DIX_TAG_READER_H__ */

