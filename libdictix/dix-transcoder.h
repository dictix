/**
 * Dictix / DixTranscoder - dix-transcoder.h
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#ifndef __DIX_TRANSCODER_H__
#define __DIX_TRANSCODER_H__

#include <glib.h>
#include <glib-object.h>
#include <gst/gst.h>

G_BEGIN_DECLS

#define DIX_TYPE_TRANSCODER            (dix_transcoder_get_type ())
#define DIX_TRANSCODER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), DIX_TYPE_TRANSCODER, DixTranscoder))
#define DIX_TRANSCODER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), DIX_TYPE_TRANSCODER, DixTranscoderClass))
#define DIX_IS_TRANSCODER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DIX_TYPE_TRANSCODER))
#define DIX_IS_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), DIX_TYPE_TRANSCODER))
#define DIX_TRANSCODER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), DIX_TYPE_TRANSCODER, DixTranscoderClass))

typedef struct _DixTranscoder          DixTranscoder;
typedef struct _DixTranscoderClass     DixTranscoderClass;
typedef struct _DixTranscoderPrivate   DixTranscoderPrivate;

struct _DixTranscoder
{
	GstPipeline parent;

	/* < private > */
	DixTranscoderPrivate *priv;
};

struct _DixTranscoderClass
{
	GstPipelineClass parent_class;

	/* < signals > */
};

GType            dix_transcoder_get_type    (void) G_GNUC_CONST;
DixTranscoder*   dix_transcoder_new         ();

void             dix_transcoder_pre_load    (DixTranscoder *transcoder, gchar *uri);
void             dix_transcoder_transcode   (DixTranscoder *transcoder, gchar *uri);

G_END_DECLS

#endif /* __DIX_TRANSCODER_H__ */

