/**
 * Dictix / DixTagWriter - dix-tag-writer.h
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#ifndef __DIX_TAG_WRITER_H__
#define __DIX_TAG_WRITER_H__

#include <glib.h>
#include <glib-object.h>
#include <gst/gst.h>

G_BEGIN_DECLS

#define DIX_TYPE_TAG_WRITER            (dix_tag_writer_get_type ())
#define DIX_TAG_WRITER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), DIX_TYPE_TAG_WRITER, DixTagWriter))
#define DIX_TAG_WRITER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), DIX_TYPE_TAG_WRITER, DixTagWriterClass))
#define DIX_IS_TAG_WRITER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DIX_TYPE_TAG_WRITER))
#define DIX_IS_TAG_WRITER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), DIX_TYPE_TAG_WRITER))
#define DIX_TAG_WRITER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), DIX_TYPE_TAG_WRITER, DixTagWriterClass))

typedef struct _DixTagWriter          DixTagWriter;
typedef struct _DixTagWriterClass     DixTagWriterClass;
typedef struct _DixTagWriterPrivate   DixTagWriterPrivate;

struct _DixTagWriter
{
	GstPipeline parent;

	/* < private > */
	DixTagWriterPrivate *priv;
};

struct _DixTagWriterClass
{
	GstPipelineClass parent_class;

	/* < signals > */
};

GType           dix_tag_writer_get_type          (void) G_GNUC_CONST;
DixTagWriter*   dix_tag_writer_new               (const gchar *uri);

void            dix_tag_writer_save              (DixTagWriter *writer);

void            dix_tag_writer_set_title         (DixTagWriter *writer, const gchar *title);

G_END_DECLS

#endif /* __DIX_TAG_WRITER_H__ */

