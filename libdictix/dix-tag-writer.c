/**
 * Dictix / DixTagWriter - dix-tag-writer.c
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <gio/gio.h>
#include <gst/gst.h>
#include <gst/pbutils/pbutils.h>

#include "dix-tag-writer.h"

enum {
	PROP_0,
	PROP_FILE_URI
};

struct _DixTagWriterPrivate
{
	gchar *uri;

	GstBus *bus;

	GstTagList *tags;
};

#define DIX_TAG_WRITER_GET_PRIVATE(obj) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), DIX_TYPE_TAG_WRITER, DixTagWriterPrivate))

G_DEFINE_TYPE (DixTagWriter, dix_tag_writer, GST_TYPE_PIPELINE)

static void
dix_tag_writer_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
	DixTagWriter *writer = (DixTagWriter *) object;
	DixTagWriterPrivate *priv = writer->priv;

	switch (prop_id) {
		case PROP_FILE_URI:
			priv->uri = g_strdup (g_value_get_string (value));

			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
dix_tag_writer_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
	DixTagWriter *writer = (DixTagWriter *) object;
	DixTagWriterPrivate *priv = writer->priv;

	switch (prop_id) {
		case PROP_FILE_URI:
			g_value_set_string (value, priv->uri);

			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
dix_tag_writer_finalize (GObject *object)
{
	DixTagWriter *writer = (DixTagWriter *) object;
	DixTagWriterPrivate *priv = writer->priv;

	g_free (priv->uri);
	priv->uri = NULL;

	if (priv->tags != NULL) {
		gst_tag_list_free (priv->tags);
	}
	priv->tags = NULL;

	G_OBJECT_CLASS (dix_tag_writer_parent_class)->finalize (object);
}

static void
dix_tag_writer_class_init (DixTagWriterClass* klass)
{
	GObjectClass *gobject_class = (GObjectClass *) klass;

	gobject_class->set_property = dix_tag_writer_set_property;
	gobject_class->get_property = dix_tag_writer_get_property;
	gobject_class->finalize = dix_tag_writer_finalize;

	g_object_class_install_property (gobject_class,
	                                 PROP_FILE_URI,
	                                 g_param_spec_string ("file-uri",
	                                "FLAC audio file uri",
	                                "Uri of the flac audio file we're about to write to",
	                                 NULL,
	                                 G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE));

	g_type_class_add_private (gobject_class, sizeof (DixTagWriterPrivate));
}

static void
dix_tag_writer_init (DixTagWriter* writer)
{
	DixTagWriterPrivate *priv = DIX_TAG_WRITER_GET_PRIVATE (writer);
	writer->priv = priv;

	gst_object_set_name (GST_OBJECT(writer), "DictixTagWriter");

	priv->uri = NULL;

	priv->tags = gst_tag_list_new ();
}

DixTagWriter*
dix_tag_writer_new (const gchar* uri)
{
	g_return_val_if_fail ((uri != NULL), NULL);
	if (g_str_has_suffix (uri, ".flac") == FALSE) {
		g_warning ("TagWriter only support FLAC audio file.\n");

		return NULL;
	}

	return g_object_new (DIX_TYPE_TAG_WRITER, "file-uri", uri, NULL);
}

void
dix_tag_writer_save (DixTagWriter *writer)
{
	g_return_if_fail (DIX_IS_TAG_WRITER (writer));
	DixTagWriterPrivate *priv = writer->priv;
	GError *error = NULL;
	gchar *filename = NULL;
	gchar *path = NULL;
	gchar *basename = NULL;
	gchar *hidden = NULL;
	GstElement *source = NULL;
	GstElement *decoder = NULL;
	GstElement *setter = NULL;
	GstElement *sink = NULL;
	GFile *file = NULL;
	GOutputStream *stream = NULL;
	gboolean done = FALSE;
	GstMessage *message;
	GFile *old = NULL;
	GFile *new = NULL;

	if (G_UNLIKELY (priv->uri == NULL)) {
		g_warning ("No file specified...\n");

		return;
	}

	if (priv->tags == NULL) {
		return;
	}

	filename = g_filename_from_uri (priv->uri, NULL, NULL);
	path = g_path_get_dirname (filename);
	basename = g_path_get_basename (filename);
	hidden = g_build_filename (path,
	                           g_strconcat (".", basename, NULL),
	                           NULL);

	file = g_file_new_for_path (hidden);
	stream = G_OUTPUT_STREAM (g_file_create (file,
	                                         G_FILE_CREATE_PRIVATE,
	                                         NULL,
	                                         &error));
	if (error != NULL) {
		g_warning ("%s\n", error->message);

		return;
	}

	if (G_LIKELY (priv->bus == NULL)) {
		source = gst_element_factory_make ("giosrc", "DictixTagWriterSource");
		if (source == NULL) {
			return;
		}
		g_object_set (G_OBJECT (source), "location", priv->uri, NULL);
		gst_bin_add (GST_BIN (writer), source);

		decoder = gst_element_factory_make ("flacdec", "DictixTagWriterDecoder");
		if (decoder == NULL) {
			gst_object_unref (source);

			return;
		}
		gst_bin_add (GST_BIN (writer), decoder);
		gst_element_link (source, decoder);

		setter = gst_element_factory_make ("flactag", "DictixTagWriterSetter");
		if (setter == NULL) {
			gst_object_unref (source);
			gst_object_unref (decoder);

			return;
		}
		gst_bin_add (GST_BIN (writer), setter);
		gst_element_link (decoder, setter);

		sink = gst_element_factory_make ("giostreamsink", "DictixTagWriterSink");
		if (sink == NULL) {
			gst_object_unref (source);
			gst_object_unref (decoder);
			gst_object_unref (setter);

			return;
		}
		gst_bin_add (GST_BIN (writer), sink);
		gst_element_link (setter, sink);

		priv->bus = gst_element_get_bus (GST_ELEMENT (writer));
	}

	g_object_set (sink, "stream", G_OUTPUT_STREAM (stream), NULL);

	gst_tag_setter_merge_tags (GST_TAG_SETTER (setter),
	                           priv->tags,
	                           GST_TAG_MERGE_REPLACE);

	gst_element_set_state (GST_ELEMENT (writer), GST_STATE_PLAYING);

	while (done) {
		message = gst_bus_timed_pop (priv->bus, GST_CLOCK_TIME_NONE);

		switch (GST_MESSAGE_TYPE (message)) {
			case GST_MESSAGE_ERROR: {
				/* TODO: Handle that... */

				break;
			} case GST_MESSAGE_EOS: {
				done = TRUE;

				break;
			} default : {
				break;
			}
		}

		gst_message_unref (message);
	}

	gst_element_set_state (GST_ELEMENT (writer), GST_STATE_NULL);

	g_output_stream_close (stream, NULL, NULL);
	g_object_unref (stream);

	old = g_file_new_for_uri (priv->uri);
	new = g_file_new_for_path (hidden);

	g_file_copy_attributes (old,
	                        new,
	                        G_FILE_COPY_ALL_METADATA,
	                        NULL,
	                        NULL);
	g_file_move (new,
	             old,
	             G_FILE_COPY_OVERWRITE,
	             NULL,
	             NULL,
	             NULL,
	             &error);

	g_free (filename);
	g_free (path);
	g_free (basename);
	g_free (hidden);
}

void
dix_tag_writer_set_title (DixTagWriter *writer, const gchar *title)
{
	g_return_if_fail (DIX_IS_TAG_WRITER (writer));
	g_return_if_fail (title != NULL);
	DixTagWriterPrivate *priv = writer->priv;

	if (G_UNLIKELY (priv->bus != NULL)) {
		return;
	}

	if (! g_utf8_validate (title, -1, NULL)) {
		g_warning ("Title should be an valid utf8 string");

		return;
	}

	gst_tag_list_add (priv->tags,
	                  GST_TAG_MERGE_REPLACE_ALL,
	                  GST_TAG_TITLE,
	                  g_strdup (title),
	                  NULL);
}

