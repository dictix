/**
 * Dictix / DixSpectrograph - dix-spectrograph.h
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#ifndef __DIX_SPECTROGRAPH_H__
#define __DIX_SPECTROGRAPH_H__

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define DIX_TYPE_SPECTROGRAPH            (dix_spectrograph_get_type ())
#define DIX_SPECTROGRAPH(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), DIX_TYPE_SPECTROGRAPH, DixSpectrograph))
#define DIX_SPECTROGRAPH_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), DIX_TYPE_SPECTROGRAPH, DixSpectrographClass))
#define DIX_IS_SPECTROGRAPH(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DIX_TYPE_SPECTROGRAPH))
#define DIX_IS_SPECTROGRAPH_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), DIX_TYPE_SPECTROGRAPH))
#define DIX_SPECTROGRAPH_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), DIX_TYPE_SPECTROGRAPH, DixSpectrographClass))

typedef struct _DixSpectrograph           DixSpectrograph;
typedef struct _DixSpectrographClass      DixSpectrographClass;
typedef struct _DixSpectrographPrivate    DixSpectrographPrivate;

struct _DixSpectrograph
{
	GtkWidget parent;

	/* < private > */
	DixSpectrographPrivate* priv;
};


struct _DixSpectrographClass
{
	GtkWidgetClass parent_class;

	/* < signals > */
};


GType        dix_spectrograth_get_type   (void) G_GNUC_CONST;
GtkWidget*   dix_spectrograph_new        ();

void   dix_spectrograph_set_magnitudes   (DixSpectrograph* spec,
                                          guint bands,
                                          gfloat* magnitudes);

G_END_DECLS

#endif /* __DIX_SPECTROGRAPH_H__ */

