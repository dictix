/**
 * Dictix / DixTranscoder - dix-transcoder.c
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <gio/gio.h>
#include <gst/gst.h>

#include "dix-transcoder.h"

enum {
	PROP_0
};

struct _DixTranscoderPrivate
{
	gchar *input_uri;
	gchar *output_uri;

	GstBus *bus;

	GstElement *source;
	GstElement *decoder;
	GstElement *encoder;
	GstElement *sink;
};

#define DIX_TRANSCODER_GET_PRIVATE(obj) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), DIX_TYPE_TRANSCODER, DixTranscoderPrivate))

G_DEFINE_TYPE (DixTranscoder, dix_transcoder, GST_TYPE_PIPELINE)

static void     dix_transcoder_error_cb             (GstBus *bus, GstMessage *message, gpointer data);
static void     dix_transcoder_element_cb           (GstBus *bus, GstMessage *message, gpointer data);
static void     dix_transcoder_state_changed_cb     (GstBus *bus, GstMessage *message, gpointer data);
static void     dix_transcoder_eos_cb               (GstBus *bus, GstMessage *message, gpointer data);

static void
dix_transcoder_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
	DixTranscoder *transcoder = (DixTranscoder *) object;
	DixTranscoderPrivate *priv = transcoder->priv;

	switch (prop_id) {
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
dix_transcoder_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
	DixTranscoder *transcoder = (DixTranscoder *) object;
	DixTranscoderPrivate *priv = transcoder->priv;

	switch (prop_id) {
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
dix_transcoder_dispose (GObject *object)
{
	DixTranscoder *transcoder = (DixTranscoder *) object;
	DixTranscoderPrivate *priv = transcoder->priv;

	if (priv->bus != NULL) {
		gst_object_unref (GST_OBJECT (priv->bus));
	}
	priv->bus = NULL;

	G_OBJECT_CLASS (dix_transcoder_parent_class)->dispose (object);
}

static void
dix_transcoder_finalize (GObject *object)
{
	DixTranscoder *transcoder = (DixTranscoder *) object;
	DixTranscoderPrivate *priv = transcoder->priv;

	if (priv->input_uri != NULL) {
		g_free (priv->input_uri);
	}
	priv->input_uri = NULL;
	if (priv->output_uri != NULL) {
		g_free (priv->output_uri);
	}
	priv->output_uri = NULL;

	G_OBJECT_CLASS (dix_transcoder_parent_class)->finalize (object);
}

static void
dix_transcoder_class_init (DixTranscoderClass* klass)
{
	GObjectClass *gobject_class = (GObjectClass *) klass;

	gobject_class->set_property = dix_transcoder_set_property;
	gobject_class->get_property = dix_transcoder_get_property;
	gobject_class->dispose = dix_transcoder_dispose;
	gobject_class->finalize = dix_transcoder_finalize;

	g_type_class_add_private (gobject_class, sizeof (DixTranscoderPrivate));
}

static void
dix_transcoder_init (DixTranscoder* transcoder)
{
	DixTranscoderPrivate *priv = DIX_TRANSCODER_GET_PRIVATE (transcoder);
	transcoder->priv = priv;

	gst_object_set_name (GST_OBJECT(transcoder), "DictixTranscoder");

	priv->input_uri = NULL;
	priv->output_uri = NULL;

	priv->bus = NULL;

	priv->source = NULL;
	priv->decoder = NULL;
	priv->encoder = NULL;
	priv->sink = NULL;
}

DixTranscoder*
dix_transcoder_new ()
{
	return g_object_new (DIX_TYPE_TRANSCODER, NULL);
}

void
dix_transcoder_pre_load (DixTranscoder *transcoder, gchar *uri)
{
	g_return_if_fail (DIX_IS_TRANSCODER (transcoder));
	g_return_if_fail (uri != NULL);
	DixTranscoderPrivate *priv = transcoder->priv;

	if (g_str_has_suffix (uri, ".flac") == FALSE) {
		g_warning ("Transcoder only handles FLAC audio files as input.");

		return;
	}

	if (G_UNLIKELY (priv->bus == NULL)) {
		priv->source = gst_element_factory_make ("giosrc", "DictixTranscoderSource");
		priv->decoder = gst_element_factory_make ("flacdec", "DictixTranscoderDecoder");
		priv->encoder = gst_element_factory_make ("encodebin", "DictixTranscoderEncoder");
		priv->sink = gst_element_factory_make ("giosink", "DictixTranscoderSink");

		if (priv->source == NULL || priv->decoder == NULL
		    || priv->encoder == NULL || priv->sink == NULL) {

			return;
		}

		gst_bin_add_many (GST_BIN (transcoder),
		                  priv->source,
		                  priv->decoder,
		                  priv->encoder,
		                  priv->sink,
		                  NULL);

		gst_element_link_many (priv->source,
		                       priv->decoder,
		                       priv->encoder,
		                       priv->sink,
		                       NULL);

		priv->bus = gst_pipeline_get_bus (GST_PIPELINE (transcoder));
		gst_bus_add_signal_watch (priv->bus);
		g_signal_connect (priv->bus, "message::error",
		                  G_CALLBACK (dix_transcoder_error_cb), transcoder);
		g_signal_connect (priv->bus, "message::element",
		                  G_CALLBACK (dix_transcoder_element_cb), transcoder);
		g_signal_connect (priv->bus, "message::state-changed",
		                  G_CALLBACK (dix_transcoder_state_changed_cb), transcoder);
		g_signal_connect (priv->bus, "message::eos",
		                  G_CALLBACK (dix_transcoder_eos_cb), transcoder);
	}

	g_object_set (G_OBJECT (priv->source), "location", uri, NULL);
}

void
dix_transcoder_transcode (DixTranscoder *transcoder, gchar *uri)
{
	g_return_if_fail (DIX_IS_TRANSCODER (transcoder));
	g_return_if_fail (uri != NULL);
	DixTranscoderPrivate *priv = transcoder->priv;


}

static void
dix_transcoder_error_cb (GstBus *bus, GstMessage *message, gpointer data)
{
	DixTranscoder *transcoder = (DixTranscoder *) data;
	DixTranscoderPrivate *priv = transcoder->priv;
	GError *error;

/*	error = g_error_new (DIX_ERROR, DIX_ERROR_GST_INTERNAL,*/
/*	                     _("Audio engine internal error"));*/
}

static void
dix_transcoder_element_cb (GstBus *bus, GstMessage *message, gpointer data)
{
	DixTranscoder *transcoder = (DixTranscoder *) data;
	DixTranscoderPrivate *priv = transcoder->priv;
	const GstStructure *structure;

	if (message->src != GST_OBJECT (priv->sink)) {
		return;
	}

	structure = gst_message_get_structure (message);
	if (gst_structure_has_name (structure, "file-exists")) {
		/* File alredy exists, shoud not happened... */
		gst_bus_set_flushing (priv->bus, TRUE);



		gst_bus_set_flushing (priv->bus, FALSE);
		gst_element_set_state (GST_ELEMENT (transcoder), GST_STATE_PLAYING);
	}
}

static void
dix_transcoder_state_changed_cb (GstBus *bus, GstMessage *message, gpointer data)
{
	DixTranscoder *transcoder = (DixTranscoder *) data;
	DixTranscoderPrivate *priv = transcoder->priv;
	GstState state, pending;
	GFile *file = NULL;

	gst_message_parse_state_changed (message, NULL, &state, NULL);
	if (message->src != GST_OBJECT (transcoder)) {
		return;
	}

	switch (state) {
		case GST_STATE_NULL: {
			break;
		} case GST_STATE_READY: {
			break;
		} case GST_STATE_PLAYING: {
			break;
		} default: {
			break;
		}
	}
}

static void
dix_transcoder_eos_cb (GstBus *bus, GstMessage *message, gpointer data)
{
	DixTranscoder *transcoder = (DixTranscoder *) data;
	DixTranscoderPrivate *priv = transcoder->priv;

	gst_element_set_state (GST_ELEMENT (transcoder), GST_STATE_NULL);
	gst_element_get_state (GST_ELEMENT (transcoder), NULL, NULL, GST_CLOCK_TIME_NONE);
}

