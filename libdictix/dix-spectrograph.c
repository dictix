/**
 * Dictix / DixSpectrograph - dix-spectrograph.c
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include "dix-spectrograph.h"

enum {
	PROP_0,
	PROP_ACTIVE,
	PROP_BANDS
};

struct _DixSpectrographPrivate
{
	gboolean active;

	guint bands;

	gfloat* magnitudes;
};

#define DIX_SPECTROGRAPH_GET_PRIVATE(obj) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), DIX_TYPE_SPECTROGRAPH, DixSpectrographPrivate))

#define DIX_SPECTROGRAPH_BANDS           8
#define DIX_SPECTROGRAPH_THRESHOLD      -70.0f
#define DIX_SPECTROGRAPH_HEIGHT          35
#define DIX_SPECTROGRAPH_HEIGHT_D        35.0
#define DIX_SPECTROGRAPH_LED_WIDTH       8
#define DIX_SPECTROGRAPH_LED_WIDTH_D     8.0
#define DIX_SPECTROGRAPH_LED_HEIGHT      5
#define DIX_SPECTROGRAPH_LED_HEIGHT_D    5.0
#define DIX_SPECTROGRAPH_LED_PADDING     1
#define DIX_SPECTROGRAPH_LED_PADDING_D   1.0

G_DEFINE_TYPE (DixSpectrograph, dix_spectrograph, GTK_TYPE_WIDGET)

static guint    dix_spectrograph_log_scale     (gfloat db);

static void
dix_spectrograph_set_property (GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec)
{
	DixSpectrograph* spec = (DixSpectrograph *) object;
	DixSpectrographPrivate* priv = spec->priv;
	gboolean active;

	switch (prop_id) {
		case PROP_ACTIVE: {
			active = g_value_get_boolean (value);

			if (priv->active != active) {
				priv->active = active;

				if (priv->active == TRUE) {
					gtk_widget_set_state_flags (GTK_WIDGET (spec),
					                            GTK_STATE_FLAG_ACTIVE, FALSE);
				} else {
					gtk_widget_unset_state_flags (GTK_WIDGET (spec),
					                              GTK_STATE_FLAG_ACTIVE);
				}

				g_object_notify (G_OBJECT (spec), "active");
			}

			break;
		} default: {
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);

			break;
		}
	}
}


static void
dix_spectrograph_get_property (GObject* object, guint prop_id, GValue* value, GParamSpec* pspec)
{
	DixSpectrograph* spec = (DixSpectrograph *) object;
	DixSpectrographPrivate* priv = spec->priv;

	switch (prop_id) {
		case PROP_ACTIVE: {
			g_value_set_boolean (value, priv->active);

			break;
		} case PROP_BANDS: {
			g_value_set_uint (value, priv->bands);

			break;
		} default: {
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);

			break;
		}
	}
}

static void
dix_spectrograph_finalize (GObject *object)
{
	DixSpectrograph* spec = (DixSpectrograph *) object;
	DixSpectrographPrivate* priv = spec->priv;

	G_OBJECT_CLASS (dix_spectrograph_parent_class)->finalize (object);
}

static void
dix_spectrograph_realize (GtkWidget* widget)
{
	GdkWindow *window = NULL;
	GtkAllocation allocation;
	GdkWindowAttr attributes;
	gint attributes_mask;

	gtk_widget_get_allocation (widget, &allocation);

	gtk_widget_set_realized (widget, TRUE);

	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.x = allocation.x;
	attributes.y = allocation.y;
	attributes.width = allocation.width;
	attributes.height = allocation.height;
	attributes.visual = gtk_widget_get_visual (widget);
	attributes.event_mask = gtk_widget_get_events (widget) | GDK_EXPOSURE_MASK;

	attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL;

	window = gdk_window_new (gtk_widget_get_parent_window (widget),
	                         &attributes, attributes_mask);
	gtk_widget_set_window (widget, window);
	gdk_window_set_user_data (window, widget);
}


static void
dix_spectrograph_get_preferred_width (GtkWidget *widget, gint *minimal, gint *natural)
{
	DixSpectrograph* spec = (DixSpectrograph*) widget;
	DixSpectrographPrivate* priv = spec->priv;
	gint width = 0;

	width = (priv->bands * DIX_SPECTROGRAPH_LED_WIDTH)
	        + ((priv->bands - 1) * DIX_SPECTROGRAPH_LED_PADDING);

	*minimal = *natural = width;
}

static void
dix_spectrograph_get_preferred_height (GtkWidget *widget, gint *minimal, gint *natural)
{
	gint height = 0;

	height = DIX_SPECTROGRAPH_HEIGHT;

	*minimal = *natural = height;
}

static gboolean
dix_spectrograph_draw (GtkWidget *widget, cairo_t *cr)
{
	DixSpectrograph* spec = (DixSpectrograph *) widget;
	DixSpectrographPrivate* priv = spec->priv;
	GtkStyleContext* context = NULL;
	GtkStateFlags state;
	gint height;
	guint level;
	gint i, j;

	context = gtk_widget_get_style_context (widget);
	state = gtk_widget_get_state_flags (widget);
	if (G_LIKELY (context != NULL)) {
		gtk_style_context_set_state (context, state);
	}

	height = gtk_widget_get_allocated_height (widget);

	for (i = 0; i < priv->bands; i++) {
		level = dix_spectrograph_log_scale (priv->magnitudes[i]);
		if (G_UNLIKELY (level > 6)) {
			level = 6;
		}

		if (level == 0) {
			cairo_rectangle (cr,
			                 (gdouble) i * DIX_SPECTROGRAPH_LED_WIDTH_D
			                  + (gdouble) i * DIX_SPECTROGRAPH_LED_PADDING_D,
			                 (gdouble) height - 1.0,
			                 DIX_SPECTROGRAPH_LED_WIDTH_D,
			                 1.0);
			cairo_set_source_rgb (cr, 0.00, 0.00, 0.00);

			cairo_fill (cr);
		} else {
			for (j = level; j > 0 ; j--) {
				cairo_rectangle (cr,
				                 (gdouble) i * DIX_SPECTROGRAPH_LED_WIDTH_D
				                  + (gdouble) i * DIX_SPECTROGRAPH_LED_PADDING_D,
				                 (gdouble) height
				                  - (gdouble) level * DIX_SPECTROGRAPH_LED_HEIGHT_D
				                  - (gdouble) (level - 1) * DIX_SPECTROGRAPH_LED_PADDING_D,
				                 DIX_SPECTROGRAPH_LED_WIDTH_D,
				                 DIX_SPECTROGRAPH_LED_HEIGHT_D);
				cairo_set_source_rgb (cr, 0.00, 0.00, 0.00);

				cairo_fill (cr);
				level--;
			}
		}
	}

	return FALSE;
}

static void
dix_spectrograph_class_init (DixSpectrographClass* klass)
{
	GObjectClass* gobject_class = (GObjectClass *) klass;
	GtkWidgetClass* widget_class = (GtkWidgetClass *) klass;

	gobject_class->set_property = dix_spectrograph_set_property;
	gobject_class->get_property = dix_spectrograph_get_property;
	gobject_class->finalize = dix_spectrograph_finalize;

	widget_class->realize = dix_spectrograph_realize;
	widget_class->get_preferred_width = dix_spectrograph_get_preferred_width;
	widget_class->get_preferred_height = dix_spectrograph_get_preferred_height;
	widget_class->draw = dix_spectrograph_draw;

	g_object_class_install_property (gobject_class,
	                                 PROP_ACTIVE,
	                                 g_param_spec_boolean ("active",
	                                 "Active",
	                                 "Whether the spectrograph is active",
	                                 FALSE,
	                                 G_PARAM_READWRITE));

	g_object_class_install_property (gobject_class,
	                                 PROP_BANDS,
	                                 g_param_spec_uint ("bands",
	                                 "Bands",
	                                 "Number of frequency bands.",
	                                 6, 12, DIX_SPECTROGRAPH_BANDS,
	                                 G_PARAM_READABLE));

	g_type_class_add_private (gobject_class, sizeof (DixSpectrographPrivate));
}

static void
dix_spectrograph_init (DixSpectrograph* spec)
{
	DixSpectrographPrivate* priv = DIX_SPECTROGRAPH_GET_PRIVATE (spec);
	spec->priv = priv;
	gint i;

	priv->active = FALSE;
	gtk_widget_unset_state_flags (GTK_WIDGET (spec),
	                              GTK_STATE_FLAG_ACTIVE);

	priv->bands = DIX_SPECTROGRAPH_BANDS;

	priv->magnitudes = g_malloc (priv->bands * sizeof (gfloat));
	for (i = 0; i < priv->bands; i++) {
		priv->magnitudes[i] = DIX_SPECTROGRAPH_THRESHOLD;
	}
/*priv->magnitudes[0] = -70.0f;*/
/*priv->magnitudes[1] = -40.0f;*/
/*priv->magnitudes[2] = -30.0f;*/
/*priv->magnitudes[3] = -20.0f;*/
/*priv->magnitudes[4] = -15.0f;*/
/*priv->magnitudes[5] = -3.0f;*/
/*priv->magnitudes[6] = -1.0f;*/
/*priv->magnitudes[7] = -80.0f;*/
}

GtkWidget*
dix_spectrograph_new ()
{
	return GTK_WIDGET (g_object_new (DIX_TYPE_SPECTROGRAPH, NULL));
}

void
dix_spectrograph_set_magnitudes (DixSpectrograph* spec, guint bands, gfloat* magnitudes)
{
	g_return_if_fail (DIX_IS_SPECTROGRAPH (spec));
	g_return_if_fail (bands == DIX_SPECTROGRAPH_BANDS);
	g_return_if_fail (magnitudes != NULL);
	DixSpectrographPrivate* priv = spec->priv;
	gint i;

	if (G_UNLIKELY (bands != priv->bands)) {
		return;
	}

	for (i = 0; i < priv->bands; i++) {
		if (G_UNLIKELY (magnitudes[i] == priv->magnitudes[i])) {
			printf ("Improbable !\n");
			continue;
		}

		if (magnitudes[i] < DIX_SPECTROGRAPH_THRESHOLD) {
			priv->magnitudes[i] = DIX_SPECTROGRAPH_THRESHOLD;
		} else if (magnitudes[i] > priv->magnitudes[i]) {
			if (magnitudes[i] > 6.0f) {
				priv->magnitudes[i] = 6.0f;
			} else {
				priv->magnitudes[i] = magnitudes[i];
			}
		} else if (magnitudes[i] < priv->magnitudes[i]) {
			priv->magnitudes[i] -= 0.75f; /* Ballistic coefficient */
		}
	}

	gtk_widget_queue_draw (GTK_WIDGET (spec));
}

static guint
dix_spectrograph_log_scale (gfloat db)
{
	guint level = 0;

	if (db <= -70.0) {
		level = 0;
	} else if (db < -37.2f) {
		level = 1;
	} else if (db < -25.8f) {
		level = 2;
	} else if (db < -17.0f) {
		level = 3;
	} else if (db < -9.4f) {
		level = 4;
	} else if (db < -1.7f) {
		level = 5;
	} else {
		level = 6;
	}

	return level;
}

