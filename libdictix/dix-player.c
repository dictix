/**
 * Dictix / DixPlayer - dix-player.c
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <gio/gio.h>
#include <gst/gst.h>

#include "dix-player.h"

enum {
	PLAYBACK_STARTED,
	PLAYBACK_STOPPED,
	PIPELINE_ERROR,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

struct _DixPlayerPrivate
{
	gboolean now_playing;
	gboolean stopping;

	GstBus *bus;

	GstElement *source;
	GstElement *decoder;
	GstElement *converter;
	GstElement *resampler;
	GstElement *sink;
};

#define DIX_PLAYER_GET_PRIVATE(obj) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), DIX_TYPE_PLAYER, DixPlayerPrivate))

G_DEFINE_TYPE (DixPlayer, dix_player, GST_TYPE_PIPELINE)

static void    dix_player_error_cb            (GstBus *bus, GstMessage *message, gpointer data);
static void    dix_player_state_changed_cb    (GstBus *bus, GstMessage *message, gpointer data);
static void    dix_player_eos_cb              (GstBus *bus, GstMessage *message, gpointer data);

static void
dix_player_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
	DixPlayer *player = (DixPlayer *) object;
	DixPlayerPrivate *priv = player->priv;

	switch (prop_id) {
		default: {
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		}
	}
}

static void
dix_player_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
	DixPlayer *player = (DixPlayer *) object;
	DixPlayerPrivate *priv = player->priv;

	switch (prop_id) {
		default: {
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		}
	}
}

static void
dix_player_dispose (GObject *object)
{
	DixPlayer *player = (DixPlayer *) object;
	DixPlayerPrivate *priv = player->priv;

	if (priv->bus != NULL) {
		gst_object_unref (GST_OBJECT (priv->bus));
	}
	priv->bus = NULL;

	G_OBJECT_CLASS (dix_player_parent_class)->dispose (object);
}

static void
dix_player_class_init (DixPlayerClass* klass)
{
	GObjectClass *gobject_class = (GObjectClass *) klass;

	gobject_class->set_property = dix_player_set_property;
	gobject_class->get_property = dix_player_get_property;
	gobject_class->dispose = dix_player_dispose;

	signals[PLAYBACK_STARTED] = g_signal_new ("playback-started",
	                                          G_TYPE_FROM_CLASS (gobject_class),
	                                          G_SIGNAL_RUN_LAST,
	                                          G_STRUCT_OFFSET (DixPlayerClass, playback_started),
	                                          NULL, NULL,
	                                          g_cclosure_marshal_VOID__VOID,
	                                          G_TYPE_NONE, 0);

	signals[PLAYBACK_STOPPED] = g_signal_new ("playback-stopped",
	                                          G_TYPE_FROM_CLASS (gobject_class),
	                                          G_SIGNAL_RUN_LAST,
	                                          G_STRUCT_OFFSET (DixPlayerClass, playback_stopped),
	                                          NULL, NULL,
	                                          g_cclosure_marshal_VOID__BOOLEAN,
	                                          G_TYPE_NONE, 1, G_TYPE_BOOLEAN);

	/*signals[PIPELINE_ERROR] = g_signal_new ("pipeline-error",
	                                        G_TYPE_FROM_CLASS (gobject_class),
	                                        G_SIGNAL_RUN_LAST,
	                                        G_STRUCT_OFFSET (DixPlayerClass, record_stopped),
	                                        NULL, NULL,
	                                        g_cclosure_marshal_VOID__POINTER,
	                                        1, DIX_ERROR, G_TYPE_NONE, 0);*/

	g_type_class_add_private (gobject_class, sizeof (DixPlayerPrivate));
}

static void
dix_player_init (DixPlayer* player)
{
	DixPlayerPrivate *priv = DIX_PLAYER_GET_PRIVATE (player);
	player->priv = priv;

	gst_object_set_name (GST_OBJECT(player), "DictixPlayer");

	priv->now_playing = FALSE;
	priv->stopping = FALSE;

	priv->bus = NULL;

	priv->source = NULL;
	priv->decoder = NULL;
	priv->converter = NULL;
	priv->resampler = NULL;
	priv->sink = NULL;
}

DixPlayer*
dix_player_new ()
{
	return g_object_new (DIX_TYPE_PLAYER, NULL);
}

void
dix_player_start_playing (DixPlayer *player, gchar* uri, GError **error) /* GError ! */
{
	g_return_if_fail (DIX_IS_PLAYER (player));
	DixPlayerPrivate *priv = player->priv;

	if (uri != NULL) {
		if (g_str_has_suffix (uri, ".flac") == FALSE) {
			g_warning ("Player only handles FLAC audio files.");

			return;
		}
	}

	if (G_UNLIKELY (priv->bus == NULL)) {
		priv->source = gst_element_factory_make ("giosrc", "DictixPlayerSource");
		priv->decoder = gst_element_factory_make ("flacdec", "DictixPlayerDecoder");
		priv->converter = gst_element_factory_make ("audioconvert", "DictixPlayerConverter");
		priv->resampler = gst_element_factory_make ("audioresample", "DictixPlayerResampler");
		priv->sink = gst_element_factory_make ("pulsesink", "DictixPlayerSink");

		if (priv->source == NULL || priv->decoder == NULL
		    || priv->converter == NULL || priv->resampler == NULL
		    || priv->sink == NULL) {
			return;
		}

		gst_bin_add_many (GST_BIN (player),
		                  priv->source,
		                  priv->decoder,
		                  priv->converter,
		                  priv->resampler,
		                  priv->sink,
		                  NULL);

		gst_element_link_many (priv->source,
		                       priv->decoder,
		                       priv->converter,
		                       priv->resampler,
		                       priv->sink,
		                       NULL);


		priv->bus = gst_pipeline_get_bus (GST_PIPELINE (player));
		gst_bus_add_signal_watch (priv->bus);
		g_signal_connect (priv->bus, "message::error",
		                  G_CALLBACK (dix_player_error_cb), player);
		g_signal_connect (priv->bus, "message::state-changed",
		                  G_CALLBACK (dix_player_state_changed_cb), player);
		g_signal_connect (priv->bus, "message::eos",
		                  G_CALLBACK (dix_player_eos_cb), player);
	}

	if (uri != NULL) {
		if (priv->now_playing == TRUE) {
			gst_element_set_state (GST_ELEMENT (player), GST_STATE_NULL);
			gst_element_get_state (GST_ELEMENT (player), NULL, NULL, -1); /*FIXME: Move it async !*/
		}

		g_object_set (G_OBJECT (priv->source), "location", uri, NULL);
	}

	gst_element_set_state (GST_ELEMENT (player), GST_STATE_PLAYING);
}

gint64
dix_player_query_duration (DixPlayer *player)
{
	g_return_val_if_fail (DIX_IS_PLAYER (player), -1);
	DixPlayerPrivate *priv = player->priv;
	GstFormat format =  GST_FORMAT_TIME;
	gint64 duration = 0;

	if (G_UNLIKELY (priv->now_playing == FALSE)) {
		return -1;
	}

	if (gst_element_query_duration (GST_ELEMENT (player), &format, &duration)) {
		if (format != GST_FORMAT_TIME) {
			return -1;
		}

		return duration / GST_SECOND;
	}

	return -1;
}

gint64
dix_player_query_position (DixPlayer *player)
{
	g_return_val_if_fail (DIX_IS_PLAYER (player), -1);
	DixPlayerPrivate *priv = player->priv;
	GstFormat format =  GST_FORMAT_TIME;
	gint64 position = 0;

	if (G_UNLIKELY (priv->now_playing == FALSE)) {
		return -1;
	}

	if (gst_element_query_position (GST_ELEMENT (player), &format, &position)) {
		if (format != GST_FORMAT_TIME) {
			return -1;
		}

		return position / GST_SECOND;
	}

	return -1;
}

gdouble
dix_player_query_progress (DixPlayer *player, gint64 *position)
{
	g_return_val_if_fail (DIX_IS_PLAYER (player), -1.0);
	DixPlayerPrivate *priv = player->priv;
	GstFormat format =  GST_FORMAT_TIME;
	gint64 current = 0;
	gint64 duration = 0;

	if (G_UNLIKELY (priv->now_playing == FALSE)) {
		return -1.0;
	}

	gst_element_query_position (GST_ELEMENT (player), &format, &current);
	gst_element_query_duration (GST_ELEMENT (player), &format, &duration);

	if (position != NULL) {
		*position = current / GST_SECOND;
	}

	return (gdouble) current / (gdouble) duration;
}

void
dix_player_move (DixPlayer *player, gdouble location)
{
	g_return_if_fail (DIX_IS_PLAYER (player));
	DixPlayerPrivate *priv = player->priv;
	GstFormat format =  GST_FORMAT_TIME;
	gint64 duration = 0;

	if (G_UNLIKELY (priv->now_playing == FALSE)) {
		return;
	}

	gst_element_query_duration (GST_ELEMENT (player), &format, &duration);

	gst_element_seek (GST_ELEMENT (player),
	                  1.0,
	                  format,
	                  GST_SEEK_FLAG_FLUSH,
	                  GST_SEEK_TYPE_SET,
	                  (gint64) ((gdouble) duration * location),
	                  GST_SEEK_TYPE_NONE,
	                  -1);
}

void
dix_player_pause (DixPlayer *player)
{
	g_return_if_fail (DIX_IS_PLAYER (player));
	DixPlayerPrivate *priv = player->priv;

	if (G_UNLIKELY (priv->now_playing == FALSE)) {
		return;
	}

	gst_element_set_state (GST_ELEMENT (player), GST_STATE_PAUSED);
}

void
dix_player_stop (DixPlayer *player)
{
	g_return_if_fail (DIX_IS_PLAYER (player));
	DixPlayerPrivate *priv = player->priv;
	GstMessage *message = NULL, *fake;

	if (G_UNLIKELY (priv->now_playing == FALSE)) {
		return;
	}

	priv->stopping = TRUE;

	gst_element_set_state (priv->source, GST_STATE_NULL);
	gst_element_get_state (priv->source, NULL, NULL, GST_CLOCK_TIME_NONE);
	gst_element_set_locked_state (priv->source, TRUE);

	/* We have to simulate eos now that source isn't streaming anymore: */
	fake = gst_message_new_eos (GST_OBJECT (priv->source));
	gst_bus_post (priv->bus, fake);

	do {
		message = gst_bus_pop (priv->bus);

		if (message != NULL) {
			gst_bus_async_signal_func (priv->bus, message, player);
		}
	} while (message != NULL);

	priv->stopping = FALSE;
}

void
dix_player_set_volume (DixPlayer *player, gdouble volume)
{
	g_return_if_fail (DIX_IS_PLAYER (player));
	g_return_if_fail (volume >= 0.0);
	DixPlayerPrivate *priv = player->priv;

	if (priv->bus != NULL) {
		g_object_set (G_OBJECT (priv->sink), "volume", volume, NULL);
	}
}

static void
dix_player_error_cb (GstBus *bus, GstMessage *message, gpointer data)
{
	DixPlayer *player = (DixPlayer *) data;
	DixPlayerPrivate *priv = player->priv;
	GError *error;

/*	error = g_error_new (DIX_ERROR, DIX_ERROR_GST_INTERNAL,*/
/*	                     _("Audio engine internal error"));*/
}

static void
dix_player_state_changed_cb (GstBus *bus, GstMessage *message, gpointer data)
{
	DixPlayer *player = (DixPlayer *) data;
	DixPlayerPrivate *priv = player->priv;
	GstState state;

	gst_message_parse_state_changed (message, NULL, &state, NULL);

	if (message->src != GST_OBJECT (player)) {
		return;
	}

	switch (state) {
		case GST_STATE_NULL: {
			break;
		} case GST_STATE_READY: {
			break;
		} case GST_STATE_PLAYING: {
			priv->now_playing = TRUE;
			g_signal_emit (player, signals[PLAYBACK_STARTED], 0);
			break;
		} case GST_STATE_PAUSED: {
			if (priv->stopping == FALSE) {
				g_signal_emit (player, signals[PLAYBACK_STOPPED], 0, FALSE);
			}
			break;
		} default: {
			break;
		}
	}
}

static void
dix_player_eos_cb (GstBus *bus, GstMessage *message, gpointer data)
{
	DixPlayer *player = (DixPlayer *) data;
	DixPlayerPrivate *priv = player->priv;

	gst_element_set_state (GST_ELEMENT (player), GST_STATE_NULL);
	gst_element_get_state (GST_ELEMENT (player), NULL, NULL, GST_CLOCK_TIME_NONE);

	priv->now_playing = FALSE;
	g_signal_emit (player, signals[PLAYBACK_STOPPED], 0, TRUE);

	gst_element_set_locked_state (priv->source, FALSE);
}

