#! /usr/bin/env python
# encoding: utf-8
import os
import sys
import gnome
import intltool
import misc

import Build
import Configure
import Options
import Utils

# Variables for 'waf dist'
NAME    = 'dictix'
APPNAME = 'Dictix'
VERSION = '0.1'

# Mandatory variables
srcdir = '.'
blddir = 'build'

def display_msg(conf, msg, status = None, color = None):
	color = 'CYAN'
	if type(status) == bool and status or status == "True":
		color = 'GREEN'
	elif type(status) == bool and not status or status == "False":
		color = 'YELLOW'
	Utils.pprint('NORMAL', "%s :" % msg.ljust(conf.line_just), sep='')
	Utils.pprint(color, status)


def set_options(opt):
	opt.tool_options('compiler_cc vala')


def configure(conf):
	conf.check_tool('compiler_cc vala')
	conf.check_tool('intltool')

	conf.check_cfg(package='glib-2.0',            uselib_store='GLIB',        atleast_version='2.28.0',  args='--cflags --libs', mandatory=True)
	conf.check_cfg(package='gobject-2.0',         uselib_store='GOBJECT',     atleast_version='2.28.0',  args='--cflags --libs', mandatory=True)
	conf.check_cfg(package='gmodule-export-2.0',  uselib_store='GMODULE',     atleast_version='2.28.0',  args='--cflags --libs', mandatory=True)
	conf.check_cfg(package='gtk+-3.0',            uselib_store='GTK',         atleast_version='3.0.0',   args='--cflags --libs', mandatory=True)
	conf.check_cfg(package='gstreamer-0.10',      uselib_store='GST',         atleast_version='0.10.20', args='--cflags --libs', mandatory=True)
	conf.check_cfg(package='gstreamer-base-0.10', uselib_store='GST_BASE',    atleast_version='0.10.20', args='--cflags --libs', mandatory=True)
	conf.check_cfg(package='gstreamer-pbutils-0.10', uselib_store='GST_PBUTILS',    atleast_version='0.10.20', args='--cflags --libs', mandatory=True)
	conf.check_cfg(package='libnotify',           uselib_store='NOTIFY',      atleast_version='0.7.0',   args='--cflags --libs', mandatory=True)

	conf.define('PACKAGE',           NAME)
	conf.define('PACKAGE_NAME',      APPNAME)
	conf.define('PACKAGE_VERSION',   VERSION)
	conf.define('PACKAGE_DATADIR',   conf.env['PREFIX'] + '/share/')
	conf.define('PACKAGE_LOCALEDIR', conf.env['PREFIX'] + '/share/locale/')
	conf.define('PACKAGE_SCHEMA',    '/org/gnome/' + NAME)
	conf.define('GETTEXT_PACKAGE',   APPNAME + '-' + VERSION)

	conf.env['PREFIX'] = os.path.abspath(os.path.expanduser(os.path.normpath(conf.env['PREFIX'])))
	conf.env.append_value('CCFLAGS', '-DHAVE_CONFIG_H --export-dynamic -Wall -lm')
	conf.env.append_value('LINKFLAGS', '-lm')
	conf.write_config_header('config.h')

	display_msg(conf, "Install prefix", conf.env['PREFIX'])

def build(bld):
	# C source files
	libdictix_sources = '''
		libdictix/dix-player.c
		libdictix/dix-recorder.c
		libdictix/dix-tag-reader.c
		libdictix/dix-tag-writer.c
		libdictix/dix-transcoder.c
		libdictix/dix-spectrograph.c
	'''

	# Vala source files
	dictix_sources = '''
		src/dix-about.vala
		src/dix-main.vala
		src/dix-preferences.vala
		src/dix-properties.vala
		src/dix-ui.vala
		src/dix-utils.vala
	'''

	# libdictix build
	obj              = bld.new_task_gen('cc', 'staticlib')
	obj.source       = libdictix_sources
	obj.includes     = ['.', './libdictix']
	obj.name         = 'libdictix_static'
	obj.target       = 'dictix_static'
	obj.install_path = ''
	obj.uselib       = 'GLIB GOBJECT GTK GST GST_BASE GST_PBUTILS'
	obj.ccflags      = ['-Wall', '-g']

	# dictix build
	obj                = bld.new_task_gen('cc', 'program')
	obj.source         = dictix_sources
	obj.includes       = ['.', 'src/', 'libdictix/']
	obj.vapi_dirs      = 'vapi/',
	obj.name           = 'Dictix'
	obj.target         = 'dictix'
	obj.vnum           = VERSION
	obj.install_path   = os.path.join('${PREFIX}', 'bin');
	obj.uselib         = 'GLIB GOBJECT GMODULE GTK GST NOTIFY'
	obj.packages       = 'glib-2.0 gobject-2.0 gtk+-3.0 gstreamer-0.10 libnotify'
	obj.packages       += ' config libdictix'
	obj.ccflags        = ['-include', 'config.h']
	obj.uselib_local   = 'libdictix_static'

#	obj                = bld.new_task_gen('cc', 'program')
#	obj.source         = 'spectrograph-test.c'
#	obj.includes       = ['.', 'libdictix/']
#	obj.name           = 'Spectrograph-test'
#	obj.target         = 'spectrograph-test'
#	obj.vnum           = VERSION
#	obj.uselib         = 'GLIB GOBJECT GTK GST'
#	obj.packages       = 'glib-2.0 gobject-2.0 gtk+-3.0 gstreamer-0.10'
#	obj.packages       += ' config libdictix'
#	obj.ccflags        = ['-include', 'config.h']
#	obj.uselib_local   = 'libdictix_static'

	bld.install_files(bld.env['DATADIR'] + "/" + NAME, 'data/main.ui')
	bld.install_files(bld.env['DATADIR'] + "/" + NAME, 'data/menu.ui')

	bld.install_files(bld.env['DATADIR'] + '/icons/hicolor/16x16/apps', 'data/icons/16x16/dictix.png')
	bld.install_files(bld.env['DATADIR'] + '/icons/hicolor/22x22/apps', 'data/icons/22x22/dictix.png')
	bld.install_files(bld.env['DATADIR'] + '/icons/hicolor/24x24/apps', 'data/icons/24x24/dictix.png')
	bld.install_files(bld.env['DATADIR'] + '/icons/hicolor/32x32/apps', 'data/icons/32x32/dictix.png')
	bld.install_files(bld.env['DATADIR'] + '/icons/hicolor/48x48/apps', 'data/icons/48x48/dictix.png')
	bld.install_files(bld.env['DATADIR'] + '/icons/hicolor/256x256/apps', 'data/icons/256x256/dictix.png')
	bld.install_files(bld.env['DATADIR'] + '/icons/hicolor/scalable/apps', 'data/icons/scalable/dictix.svg')

	obj = bld.new_task_gen(features = 'subst', source = 'data/dictix.desktop.in')
	bld.install_files(bld.env['DATADIR'] + '/applications', 'data/dictix.desktop')

def shutdown():
	gnome.postinstall_icons()
