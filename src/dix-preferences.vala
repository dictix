/**
 * Dictix / DixPreferences - dix-preferences.vala
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

using GLib;
using Gtk;
using Dix;

namespace Global {
	private const string NOTIFICATION_POLICY = "show-recording-notification";
}

namespace Dictix {
	public class Preferences : Dialog {
		public Preferences (Builder builder) {
			Alignment preferences_alignment = null;
			Box content_area = null;

			this.set_resizable (false);
			this.set_border_width (5);
			this.add_button (Stock.CLOSE, ResponseType.CLOSE);
			this.set_default_response (ResponseType.CLOSE);
			this.delete_event.connect (hide_on_delete);
			this.response.connect (on_response_cb);

			content_area = this.get_content_area () as Box;
			preferences_alignment = builder.get_object ("preferences-alignment") as Alignment;
			content_area.add (preferences_alignment as Widget);
		}

		public void on_response_cb (int response) {
			switch (response) {
				case ResponseType.CLOSE: {
					this.hide ();

					break;
				} default : {
					break;
				}
			}
		}
	}
}

