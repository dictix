/**
 * Dictix / DixUtils - dix-utils.vala
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

using GLib;
using Gtk;

namespace Utils {
	public const string[] size_units = {
		N_("bytes"),
		N_("Kio"),
		N_("Mio"),
		N_("Gio")
	};

	public string codec_to_string (string codec) {
		if (codec == "FLAC") {
			return _("FLAC (lossless)");
		} else if (codec == "Vorbis") {
			return _("Vorbis (lossy)");
		}

		return _("n/a");
	}

	public string basename_from_uri (string uri) {
		string path = null;

		if (uri != null) {

			try {
				path = Filename.from_uri (uri);
			} catch (Error error) {
				warning (error.message);

				return _("n/a");
			}

			return Path.get_basename (path);
		}

		return _("n/a");
	}

	public string size_to_string (uint64 bytes) {
		float size = 0.0f;
		short i = 0;

		if (bytes > 0) {
			size = (float) bytes;

			while (size > 1000.0f) {
				size /= 1024.0f;

				i++;
			}

			size *= 10.0f;
			size = Math.roundf (size);
			size /= 10.0f;

			return size.to_string ("%.01lf") + " " + size_units[i];
		}

		return size.to_string ("%.01lf") + " " + size_units[0];
	}

	public string duration_to_string (int64 duration) {
		int secs, mins, hours;

		if (duration > 0) {
			/* Computation of secs / mins / hours : */
			secs = (int) (duration % 60);
			duration -= secs;

			mins = (int) ((duration % (60 * 60)) / 60);
			duration -= mins * 60;

			if (duration == 0) {
				return mins.to_string () + ":" + secs.to_string ("%02d");
			}

			hours = (int) (duration / (60 * 60));

			return hours.to_string () + ":" + mins.to_string ("%02d") + ":" + secs.to_string ("%02d");
		}

		return "0:00";
	}

	public string channels_to_string (int channels) {
		if (channels > 0) {
			return channels.to_string ();
		}

		return _("n/a");
	}

	public string sample_rate_to_string (int sample_rate) {
		float rate = 0.0f;
		int hundreds;

		rate = ((float) sample_rate) / 1000.0f;

		if (rate > 0.0f) {
			hundreds = (int) (sample_rate % 1000);

			if ((hundreds % 100) == 0) {
				return rate.to_string ("%.01lf") + " " + _("kHz");
			}

			if ((hundreds % 10) == 0) {
				return rate.to_string ("%.02lf") + " " + _("kHz");
			}

			return rate.to_string ("%.03lf") + " " + _("kHz");
		}

		return _("n/a");
	}

	public string bit_rate_to_string (int bit_rate) {
		int kbytes = 0;

		if (bit_rate > 0) {
			kbytes = (int) (bit_rate / 1000);

			return kbytes.to_string () + " " + _("kbps");
		}

		return _("n/a");
	}
}

