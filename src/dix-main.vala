/**
 * Dictix / DixMain - dix-main.vala
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

using GLib;
using Gtk;
using Dix;

public enum State {
	UNKNOWN,
	WORKING,
	READY
}

namespace Global {
	/* App state */
	public State state;
	public bool now_recording;
	public bool now_playing;
	public bool transport_is_paused;
	public bool seeking;

	/* Last just-recorded record pointer, use once to set TreeView state */
	public TreeIter last_record;

	/* Timer value use for notification while recording */
	public int notification_timer;

	/* Usefull directories and files */
	public string data_dir;
	public string ui_file;
	public string menu_file;

	/* Main objects: */
	public Recorder recorder;
	public Player player;
//	public Transcoder transcoder;

	/* Record list, iter atributes are thoses:
	    - 0: uri (string),
	    - 1: timeval, (uint64);
	    - 2: icon (string),
	    - 3: title (string),
	    - 4: codec (string),
	    - 5: size (uint64),
	    - 6: duration (int64).
	    - 7: channels (int).
	    - 8: sample-rate (int).
	    - 9: bit-rate (int). */
	public ListStore records;

	/* Settings engine */
	public GLib.Settings settings;
}

namespace Dictix {
	public class Main : Gtk.Application {
		private static Ui ui;
		private Cancellable guardian;

		private short destroy_count;

		public Main (string id, ApplicationFlags flags) {
			GLib.Object (application_id: id, flags: flags);

			ui = null;
			guardian = null;

			destroy_count = -1;
		}

		public void on_record_started_cb () {
			if (Global.state == State.READY) {
				Global.now_recording = true;
			}
		}

		public void on_playback_started_cb () {
			if (Global.state == State.READY) {
				Global.now_playing = true;
				Global.transport_is_paused = false;
			}
		}

		public void on_record_stopped_cb (string uri) {
			if (Global.state == State.READY) {
				Global.now_recording = false;

				if (uri != null) {
					Global.last_record = add_record (uri);
				}
			}

			if (destroy_count > 0) {
				destroy_count--;
				if (destroy_count == 0) {
					just_quit ();
				}
			}
		}

		public void on_playback_stopped_cb (bool eos) {
			if (Global.state == State.READY) {
				Global.now_playing = false;

				if (eos == false) {
					Global.transport_is_paused = true;
				} else {
					Global.transport_is_paused = false;
				}
			}

			if (destroy_count > 0) {
				destroy_count--;
				if (destroy_count == 0) {
					just_quit ();
				}
			}
		}

		public void on_destroy_cb () {
			destroy_count = 0;

			if (Global.now_recording == true) {
				destroy_count++;
				Global.recorder.stop_recording ();
			}
			if (Global.now_playing == true) {
				destroy_count++;
				Global.player.stop ();
			}

			if (destroy_count == 0) {
				/* We are nither recording nor playing */
				just_quit ();
			}
		}

		private void just_quit () {
			switch (Global.state) {
				case State.WORKING:
					if (guardian != null) {
						guardian.cancel ();
					}
					break;
				default:
					break;
			}

			Gtk.main_quit ();
		}

		/* timeval = uint64.MAX is just a hack to keep new records at the beginning of the list... */
		private TreeIter add_record (string uri, uint64 timeval = uint64.MAX) {
			TagReader reader = null;
			string title = null;
			string codec = null;
			uint64 size = 0;
			int64 duration = 0;
			int channels = 0;
			int sample_rate = 0;
			int bit_rate = 0;
			TreeIter iter;

			reader = new TagReader (uri);
			Global.records.prepend (out iter);

			if (reader != null) {
				reader.load ();

				title = reader.get_title ();
				codec = reader.get_codec ();

				size = reader.get_size ();
				duration = reader.get_duration ();
				channels = reader.get_channels ();
				sample_rate = reader.get_sample_rate ();
				bit_rate = reader.get_bit_rate ();
			} else {
				title = _("Unknow record");
			}

			Global.records.set (iter,
			                    Column.URI, uri.dup (),
			                    Column.TIMEVAL, timeval,
			                    Column.PIXBUF, null,
			                    Column.TITLE, title,
			                    Column.CODEC, codec,
			                    Column.SIZE, size,
			                    Column.DURATION, duration,
			                    Column.CHANNELS, channels,
			                    Column.SAMPLE_RATE, sample_rate,
			                    Column.BIT_RATE, bit_rate);

			return iter;
		}

		private async void list_data_directory () {
			FileEnumerator enumeration = null;
			File directory = null;
			uint64 timeval = 0;
			TreeIter iter;

			directory = File.new_for_path (Global.data_dir);
			try {
				guardian = new Cancellable ();
				enumeration = yield directory.enumerate_children_async (FILE_ATTRIBUTE_STANDARD_NAME +
				                                                        "," +
				                                                        FILE_ATTRIBUTE_TIME_MODIFIED,
				                                                        FileQueryInfoFlags.NONE,
				                                                        Priority.DEFAULT,
				                                                        guardian);
				/* FIXME: Why is G_FILE_ATTRIBUTE_TIME_CREATED always == 0 ?? */
				while (true) {
					List<FileInfo> files = null;

					files = yield enumeration.next_files_async (10, Priority.DEFAULT);
					if (files != null) {
						foreach (var info in files) {
							if (info.get_name ().has_suffix (".flac") == true) {
								string filename = null;
								string uri = null;

								filename = Path.build_filename (Global.data_dir, info.get_name ());
								uri = Filename.to_uri (filename, null);
								timeval = info.get_attribute_uint64 (FILE_ATTRIBUTE_TIME_MODIFIED);

								if (uri != null) {
									add_record (uri, timeval);
								}
							}
						}
					} else {
						/* No more files, just give last record creation time to the Recorder for naming stuff: */
						if (Global.records.get_iter_first (out iter) == true) {
							Global.records.get (iter, Column.TIMEVAL, out timeval);
							if (timeval != 0) {
								Global.recorder.set ("last-record-timeval", timeval);
							}
						}

						Global.state = State.READY;

						break;
					}
				}
			} catch (Error error) {
				warning (error.message);
			}
		}

		public void on_activate_cb () {
			if (this.get_windows () != null) {
				/* An other instance alredy exists, just show it */
				ui.present ();
			} else {
				Builder builder = null;
				UIManager manager = null;

				Global.state = State.WORKING;
				Global.now_recording = false;
				Global.now_playing = false;
				Global.transport_is_paused = false;
				Global.seeking = false;
				Global.notification_timer = NotificationDelay.INVALID;

				Environment.set_variable ("PULSE_PROP_media.role", "production", true);

				Global.records = new ListStore (Column.NB,
				                                typeof (string), /* uri */
				                                typeof (uint64), /* timeval */
				                                typeof (string), /* icon */
				                                typeof (string), /* title */
				                                typeof (string), /* codec */
				                                typeof (uint64), /* size */
				                                typeof (int64), /* duration */
				                                typeof (int), /* channels */
				                                typeof (int), /* sample-rate */
				                                typeof (int)); /* bit-rate */
				Global.records.set_sort_column_id (Column.TIMEVAL, SortType.DESCENDING);

				Environment.set_application_name (Config.PACKAGE_NAME);

				//Global.settings = new GLib.Settings (Config.PACKAGE_SCHEMA);

				Global.recorder = new Recorder (Global.data_dir);
				if (Global.recorder == null) {
					warning (_("Impossible to allocate core recorder object !"));

					return;
				}
				Global.recorder.record_started.connect (on_record_started_cb);
				Global.recorder.record_stopped.connect (on_record_stopped_cb);

				Global.player = new Player ();
				if (Global.player == null) {
					warning (_("Impossible to allocate core player object !"));

					return;
				}
				Global.player.playback_started.connect (on_playback_started_cb);
				Global.player.playback_stopped.connect (on_playback_stopped_cb);

//				Global.transcoder = new Transcoder ();
//				if (Global.transcoder == null) {
//					warning (_("Impossible to allocate core transcoder object !"));

//					return;
//				}

				builder = new Builder ();
				try {
					builder.add_from_file (Global.ui_file);
				} catch (Error error) {
					warning (error.message);

					return;
				}

				manager = new UIManager ();
				try {
					manager.add_ui_from_file (Global.menu_file);
				} catch (Error error) {
					warning (error.message);

					return;
				}

				ui = new Ui (builder, manager);
				ui.destroy.connect (this.on_destroy_cb);

				list_data_directory.begin ();

				ui.set_application (this);
			}
		}

	}

	public int main (string[] args) {
		Main app = null;
		File directory = null;

		Gtk.init (ref args);

		Intl.bindtextdomain (Config.GETTEXT_PACKAGE, Config.PACKAGE_LOCALEDIR);
		Intl.bind_textdomain_codeset (Config.GETTEXT_PACKAGE, "UTF-8");
		Intl.textdomain (Config.GETTEXT_PACKAGE);

		Global.data_dir = Path.build_filename (Environment.get_user_data_dir (), Config.PACKAGE);
//		Global.ui_file = Path.build_filename (Config.PACKAGE_DATADIR, Config.PACKAGE, "main.ui");
//		Global.menu_file = Path.build_filename (Config.PACKAGE_DATADIR, Config.PACKAGE, "menu.ui");
		Global.ui_file = Path.build_filename (".", "data", "main.ui");
		Global.menu_file = Path.build_filename (".", "data", "menu.ui");

		directory = File.new_for_path (Global.data_dir); 
		if (directory.query_exists () == false) {
			/* Our xdg data dir doesn't exists, let's create it */
			try {
				directory.make_directory ();
			} catch (Error error) {
				warning (error.message);

				return 1;
			}
		}

		Gst.init (ref args);

		Notify.init (Config.PACKAGE_NAME);

		app = new Main ("org.gnome." + Config.PACKAGE_NAME, ApplicationFlags.FLAGS_NONE);
		app.activate.connect (app.on_activate_cb);

		int status = app.run (args);

		Notify.uninit ();
		Gst.deinit ();

		return status;
	}
}

