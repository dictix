/**
 * Dictix / DixProperties - dix-properties.vala
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

using GLib;
using Gtk;
using Dix;

namespace Dictix {
	public class Properties : Dialog {
		private Label title_label;
		private Label codec_label;
		private Label filename_label;
		private Label size_label;
		private Label duration_label;
		private Label channels_label;
		private Label sample_rate_label;
		private Label bit_rate_label;

		public Properties (Builder builder) {
			Alignment properties_alignment = null;
			Box content_area = null;

			this.set_resizable (false);
			this.set_border_width (5);
			this.add_button (Stock.CLOSE, ResponseType.CLOSE);
			this.set_default_response (ResponseType.CLOSE);
			this.delete_event.connect (hide_on_delete);
			this.response.connect (on_response_cb);

			content_area = this.get_content_area () as Box;
			properties_alignment = builder.get_object ("properties-alignment") as Alignment;
			content_area.add (properties_alignment as Widget);

			title_label = builder.get_object ("title-prop-label") as Label;
			codec_label = builder.get_object ("codec-prop-label") as Label;
			filename_label = builder.get_object ("filename-prop-label") as Label;
			size_label = builder.get_object ("size-prop-label") as Label;
			duration_label = builder.get_object ("duration-prop-label") as Label;
			channels_label = builder.get_object ("channels-prop-label") as Label;
			sample_rate_label = builder.get_object ("sample-rate-prop-label") as Label;
			bit_rate_label = builder.get_object ("bit-rate-prop-label") as Label;
		}

		public void on_response_cb (int response) {
			switch (response) {
				case ResponseType.CLOSE: {
					this.hide ();

					break;
				} default : {
					break;
				}
			}
		}

		public void set_record (TreeModel model, TreeIter iter) {
			string uri = null;
			string title = null;
			string codec = null;
			uint64 size = 0;
			int64 duration = 0;
			int channels = 0;
			int sample_rate = 0;
			int bit_rate = 0;

			if (model != null) {
				model.get (iter,
				           Column.URI, out uri,
				           Column.TITLE, out title,
				           Column.CODEC, out codec,
				           Column.SIZE, out size,
				           Column.DURATION, out duration,
				           Column.CHANNELS, out channels,
				           Column.SAMPLE_RATE, out sample_rate,
				           Column.BIT_RATE, out bit_rate);
			}

			title_label.set_text (title);
			codec_label.set_text (Utils.codec_to_string (codec));

			filename_label.set_text (Utils.basename_from_uri (uri));
			size_label.set_text (Utils.size_to_string (size));
			duration_label.set_text (Utils.duration_to_string (duration));
			channels_label.set_text (Utils.channels_to_string (channels));
			sample_rate_label.set_text (Utils.sample_rate_to_string (sample_rate));
			bit_rate_label.set_text (Utils.bit_rate_to_string (bit_rate));
		}

		public void reset () {
			title_label.set_text (_("Unknow record"));
			codec_label.set_text (_("n/a"));

			filename_label.set_text (_("n/a"));
			size_label.set_text (_("n/a"));
			duration_label.set_text (_("n/a"));
			channels_label.set_text (_("n/a"));
			sample_rate_label.set_text (_("n/a"));
			bit_rate_label.set_text (_("n/a"));
		}
	}
}

