/**
 * Dictix / DixUi - dix-ui.vala
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

using GLib;
using Gtk;
using Notify;
using Dix;

namespace Dictix {
	private enum Column {
		URI,
		TIMEVAL,
		PIXBUF,
		TITLE,
		CODEC,
		SIZE,
		DURATION,
		CHANNELS,
		SAMPLE_RATE,
		BIT_RATE,
		NB
	}

public enum NotificationDelay {
	SHORT = 600,
	NORMAL = 3000,
	INVALID = -1
}

	public class Ui : Window {
		private State last_state;
		private bool was_recording;
		private bool was_playing;

		private TreeIter now_playing_iter;
		private TreeIter last_played_iter;

		private Gtk.Action save_as_action;
		private Gtk.Action delete_action;
		private Gtk.Action properties_action;

		private Properties properties_dialog;

		private Preferences preferences_dialog;

		private Gtk.Action play_pause_action;
		private Gtk.Action previous_action;
		private Gtk.Action next_action;
		private Gtk.Action record_stop_action;

		private Image play_image;
		private Image pause_image;
		private Adjustment scale_adjustment;
		private Scale seek_scale;
		private Label position_label;
		private Label duration_label;

		private Spectrograph spectrograph;
		private Label status_label;
		private Button record_stop_button;

		private Spinner spinner;
		private Label no_records_label;
		private TreeView tree_view;

		private FileFilter file_filter;

		public Ui (Builder builder, UIManager manager) {
			AccelGroup accel_group = null;
			CellRenderer renderer = null;
			CellRendererText editable = null;
			TreeViewColumn column = null;
			TreeSelection selection = null;
			Box box = null;

			last_state = State.UNKNOWN;
			was_recording = true;
			was_playing = true;

			Window.set_default_icon_name (Config.PACKAGE);
			this.set_title (Config.PACKAGE);
			this.set_default_size (400, 280);
//			this.set_resizable (false); /*TODO: fix size ? */
			accel_group = manager.get_accel_group ();
			if (accel_group != null) {
				this.add_accel_group (accel_group);
			}

			box = new Box (Orientation.VERTICAL, 0);
			manager.insert_action_group (builder.get_object ("action-group") as Gtk.ActionGroup, 0);
			box.pack_start (manager.get_widget ("/menubar") as Widget, false, false, 0);
			box.pack_start (builder.get_object ("main-box") as Widget, true, true, 0);
			this.add (box as Widget);

			save_as_action = builder.get_object ("save-as-action") as Gtk.Action;
			delete_action = builder.get_object ("delete-action") as Gtk.Action;
			properties_action = builder.get_object ("properties-action") as Gtk.Action;

			file_filter = builder.get_object ("file-filter") as FileFilter;
			file_filter.set_name (_("Audio files"));

			properties_dialog = new Properties (builder);
			properties_dialog.set_transient_for (this as Window);

			preferences_dialog = new Preferences (builder);

			play_pause_action = builder.get_object ("play-pause-action") as Gtk.Action;
			previous_action = builder.get_object ("previous-action") as Gtk.Action;
			next_action = builder.get_object ("next-action") as Gtk.Action;
			record_stop_action = builder.get_object ("record-stop-action") as Gtk.Action;

			play_image = builder.get_object ("play-image") as Image;
			pause_image = builder.get_object ("pause-image") as Image;
			scale_adjustment = builder.get_object ("scale-adjustment") as Adjustment;
			seek_scale = builder.get_object ("seek-scale") as Scale;
			position_label = builder.get_object ("position-label") as Label;
			duration_label = builder.get_object ("duration-label") as Label;

			spectrograph = new Spectrograph ();
			status_label = builder.get_object ("status-label") as Label;
			record_stop_button = builder.get_object ("record-stop-button") as Button;

			spinner = builder.get_object ("spinner") as Spinner;
			no_records_label = builder.get_object ("no-records-label") as Label;
			tree_view = builder.get_object ("tree-view") as TreeView;

			box = builder.get_object ("recorder-box") as Box;
			box.pack_start (spectrograph, false, false, 0);
			box.reorder_child (spectrograph, 0);

			tree_view.set_model (Global.records as TreeModel);

			column = new TreeViewColumn ();
			column.set_title (_("State"));
			column.set_min_width (36); /* FIXME; get rid of this hack, columns should auto-size... */
			column.set_expand (false);
			column.set_resizable (false);
			renderer = new CellRendererPixbuf ();
			renderer.set ("stock-size", IconSize.MENU, "xpad", 8);
			column.pack_start (renderer, true);
			column.set_attributes (renderer, "stock-id", Column.PIXBUF);
			tree_view.append_column (column);

			column = new TreeViewColumn ();
			column.set_title (_("Title"));
			column.set_expand (true);
			column.set_resizable (false);
			editable = new CellRendererText ();
			editable.set ("editable", true);
			editable.edited.connect (on_cell_edited_cb);
			column.pack_start (editable, true);
			column.set_attributes (editable, "text", Column.TITLE);
			tree_view.append_column (column);

			column = new TreeViewColumn ();
			column.set_title (_("Duration"));
			column.set_expand (false);
			column.set_resizable (false);
			renderer = new CellRendererText ();
			renderer.set ("xpad", 8);
			column.set_cell_data_func (renderer, on_duration_cell_render_cb);
			column.pack_end (renderer, false);
			tree_view.append_column (column);

			selection = tree_view.get_selection ();
			selection.changed.connect (on_selection_changed_cb);

			builder.connect_signals (this);
			Timeout.add (100, on_timeout_cb);
			this.show_all ();
		}

		public void on_duration_cell_render_cb (CellLayout layout, CellRenderer renderer, TreeModel model, TreeIter iter)  {
			int64 duration = 0;

			model.get (iter, Column.DURATION, out duration);
			renderer.set ("text", Utils.duration_to_string (duration));
		}

		public void on_cell_edited_cb (string path, string text) {
			TagWriter writer = null;
			TreeModel model = null;
			TreeSelection selection = null;
			string uri = null;
			string title = null;
			TreeIter iter;

			model = tree_view.get_model ();
			selection = tree_view.get_selection ();

			if (unlikely (model == null || selection == null)) {
				return;
			}

			selection.get_selected (null, out iter);
			model.get (iter, Column.URI, out uri);
			model.get (iter, Column.TITLE, out title);

			if (unlikely (uri == null)) {
				return;
			}

			if (title != null && text != null) {
				if (title != text) {
					writer = new TagWriter (uri);

					writer.set_title (text);
					writer.save ();

					Global.records.set (iter, Column.TITLE, text);
				}
			}
		}

		public void update_ui_state (State state) {
			switch (state) {
				case State.WORKING:
					save_as_action.set_sensitive (false);
					delete_action.set_sensitive (false);
					properties_action.set_sensitive (false);

					play_pause_action.set_sensitive (false);
					previous_action.set_sensitive (false);
					next_action.set_sensitive (false);
					record_stop_action.set_sensitive (false);

					seek_scale.set_sensitive (false);

					spinner.show ();
					spinner.start();
					no_records_label.hide ();
					tree_view.hide ();
				break;
				case State.READY:
					play_pause_action.set_sensitive (true);

					status_label.set_text (_("Ready"));
					record_stop_action.set_sensitive (true);

					spinner.hide ();
					spinner.stop();
					if (Global.records != null) {
						no_records_label.hide ();
						tree_view.show ();
					} else {
						no_records_label.show ();
						tree_view.hide ();
					}
				break;
				default:
				break;
			}
		}

		public void on_notification_action_cb (Notification notification, string action) {
			if (unlikely (Global.state != State.READY)) {
				return;
			}

			if (action == "stop") {
				if (Global.now_recording == true) {
					Global.recorder.stop_recording ();
				}
			}
		}

		public bool on_timeout_cb () {
			Notification notification = null;
			double progress = 0.0;
			int64 position = 0;
			int64 duration = 0;
			TreePath path = null;
			TreeIter iter;

			if (unlikely (Global.state != last_state)) {
				update_ui_state (Global.state);
				last_state = Global.state;
			}

			if (unlikely (Global.state == State.WORKING)) {
				return true;
			}

			if (Global.records.get_iter_first (out iter) == false) {
				save_as_action.set_sensitive (false);
				delete_action.set_sensitive (false);
				properties_action.set_sensitive (false);

				play_pause_action.set_sensitive (false);
				previous_action.set_sensitive (false);
				next_action.set_sensitive (false);

				seek_scale.set_sensitive (false);

				no_records_label.show ();
				tree_view.hide ();
			} else {
				no_records_label.hide ();
				tree_view.show ();

				play_pause_action.set_sensitive (true);
			}

			if (Global.now_recording != was_recording) {
				was_recording = Global.now_recording;

				if (Global.now_recording == true) {
					status_label.set_text (_("Now recording..."));
					record_stop_button.set_label (_("Stop"));

					try {
						notification = new Notification (_("Now recording audio..."),
						                                 null,
						                                 Config.PACKAGE);
						notification.set_urgency (Urgency.NORMAL);
						notification.add_action ("stop",
						                         _("Stop"),
						                         on_notification_action_cb);
						notification.add_action ("ingnore",
						                         _("Ignore"),
						                         on_notification_action_cb);
						notification.show ();
						Global.notification_timer = NotificationDelay.SHORT;
					} catch (Error error) {
						warning (error.message);
					}
				} else {
					status_label.set_text (_("Ready"));
					record_stop_button.set_label (_("Record"));
					Global.notification_timer = NotificationDelay.INVALID;

					if (Global.last_record.stamp != 0) {
						tree_view.grab_focus ();
						/* FIXME: Looks a bit dirty don't you think ? */
						path = tree_view.get_model ().get_path (Global.last_record);
						tree_view.set_cursor (path,
						                      tree_view.get_column (Column.TITLE),
						                      true);

						Global.last_record.stamp = 0;
					}
				}
			}

			if (Global.now_recording == true) {
				if (Global.notification_timer >= 0) {
					Global.notification_timer--;

					if (Global.notification_timer == 0) {
						if (notification == null) {
							try {
								notification = new Notification (_("Reminder : audio is still recording !"),
									                               null,
									                               Config.PACKAGE);
								notification.set_urgency (Urgency.NORMAL);
								notification.add_action ("stop",
										                     _("Stop"),
										                     on_notification_action_cb);
								notification.add_action ("ingnore",
										                     _("Ignore"),
										                     on_notification_action_cb);
								notification.show ();
							} catch (Error error) {
								warning (error.message);
							}
						}

						Global.notification_timer = NotificationDelay.NORMAL;
					}
				}
			}

			if (now_playing_iter != last_played_iter) {
				if (last_played_iter.stamp != 0) {
					Global.records.set (last_played_iter, Column.PIXBUF, null);
				}
				Global.records.set (now_playing_iter, Column.PIXBUF, Stock.MEDIA_PLAY);

				last_played_iter = now_playing_iter;

				Global.records.get (now_playing_iter, Column.DURATION, out duration);
				duration_label.set_text (Utils.duration_to_string (duration));
			}

			if (Global.seeking == false) {
				/* Lot's of nasty signal emition during seeking ... */
				if (Global.now_playing != was_playing) {
					was_playing = Global.now_playing;

					if (Global.now_playing == true) {
						play_image.hide ();
						pause_image.show ();
						Global.records.set (now_playing_iter, Column.PIXBUF, Stock.MEDIA_PLAY);

					} else {
						play_image.show ();
						pause_image.hide ();
						if (Global.transport_is_paused == false) {

							scale_adjustment.set_value (0.0);
							position_label.set_text ("0:00");
							if (now_playing_iter.stamp != 0) {
								Global.records.set (now_playing_iter, Column.PIXBUF, null);
							}
						} else {
							Global.records.set (now_playing_iter, Column.PIXBUF, Stock.MEDIA_PAUSE);
						}
					}
				}

				if (Global.now_playing == true) {
					progress = Global.player.query_progress (out position);

					if (position >= 0 && progress >= 0.0) {
						scale_adjustment.set_value (progress);
						position_label.set_text (Utils.duration_to_string (position));
					}
				}
			}

			return true;
		}

		[CCode (instance_pos = -1)]
		public void on_save_as_cb (Gtk.Action source) {
			FileChooserDialog chooser = null;
			int response;

			if (unlikely (Global.state != State.READY)) {
				return;
			}

			chooser = new FileChooserDialog (_("Save as..."),
			                                 this as Window,
			                                 FileChooserAction.SAVE,
			                                 Stock.CANCEL, ResponseType.CANCEL,
			                                 Stock.SAVE, ResponseType.ACCEPT);
			chooser.set_do_overwrite_confirmation (true);
			chooser.add_filter (file_filter);

			response = chooser.run ();

			if (response == ResponseType.ACCEPT) {

			}

			chooser.destroy ();
		}

		[CCode (instance_pos = -1)]
		public void on_delete_action_cb (Gtk.Action source) {
			TreeModel model = null;
			TreeSelection selection = null;
			File file = null;
			string uri = null;
			TreeIter iter;

			if (unlikely (Global.state != State.READY)) {
				return;
			}

			model = tree_view.get_model ();
			selection = tree_view.get_selection ();

			if (unlikely (model == null || selection == null)) {
				return;
			}

			if (selection.count_selected_rows () != 0) {
				selection.get_selected (null, out iter);
				model.get (iter, Column.URI, out uri);

				if (Global.now_playing == true || Global.transport_is_paused == true) {
					if (iter == now_playing_iter) {
						Global.player.stop ();

						now_playing_iter.stamp = 0;
						last_played_iter.stamp = 0;
					}
				} else {
					/* Avoid access of an invlid iter in the timeout_cb, seems to be working. */
					if (iter == now_playing_iter) {
						/* Should not a happenned but bugs come from here... */
						now_playing_iter.stamp = 0;
					}
					if (iter == last_played_iter) {
						last_played_iter.stamp = 0;
					}
				}

				file = File.new_for_uri (uri);
				if (file != null) {
					try {
						file.delete ();

						Global.records.remove (iter);
					} catch (Error error) {
						warning (_("Impossible to delete %s"), uri);
					}
				}
			}
		}

		[CCode (instance_pos = -1)]
		public void on_properties_action_cb (Gtk.Action source) {
			TreeModel model = null;
			TreeSelection selection = null;
			TreeIter iter;

			if (unlikely (Global.state != State.READY)) {
				return;
			}

			model = tree_view.get_model ();
			selection = tree_view.get_selection ();

			if (unlikely (model == null || selection == null)) {
				return;
			}

			if (selection.count_selected_rows () != 0) {
				selection.get_selected (null, out iter);
				properties_dialog.set_record (model, iter);
			} else {
				properties_dialog.reset ();
			}

			properties_dialog.run ();
		}

		[CCode (instance_pos = -1)]
		public void on_quit_action_cb (Gtk.Action source) {
			this.destroy ();
		}

		[CCode (instance_pos = -1)]
		public void on_sound_levels_action_cb (Gtk.Action source) {
			string[] argv = {null, null, null, null};

			argv[0] = Environment.find_program_in_path ("gnome-control-center");
			argv[1] = "sound";
//			argv[2] = "--page";
//			argv[3] = "input";

			try {
				Process.spawn_async (null, argv, null, 0, null, null);
			} catch (Error error) {
				warning (error.message);
			}
		}

		[CCode (instance_pos = -1)]
		public void on_preferences_action_cb (Gtk.Action source) {
			preferences_dialog.run ();
		}

		[CCode (instance_pos = -1)]
		public void on_previous_action_cb (Gtk.Action source) {
			TreeModel model = null;
			TreeSelection selection = null;
			string uri = null;
			TreeIter iter;

			if (unlikely (Global.state != State.READY)) {
				return;
			}

			model = tree_view.get_model ();
			selection = tree_view.get_selection ();

			if (unlikely (model == null || selection == null)) {
				return;
			}

			if (selection.count_selected_rows () != 0) {
				selection.get_selected (null, out iter);
				if (model.iter_previous (ref iter) == true) {
					selection.select_iter (iter);

					if (Global.now_playing == true) {
						model.get (iter, Column.URI, out uri);

						Global.player.start_playing (uri);
						now_playing_iter = iter;
					}
				}
			}
		}

		[CCode (instance_pos = -1)]
		public void on_play_pause_action_cb (Gtk.Action source) {
			TreeModel model = null;
			TreeSelection selection = null;
			string uri = null;
			TreeIter iter;

			if (unlikely (Global.state != State.READY)) {
				return;
			}

			if (Global.now_playing == false) {
				if (Global.transport_is_paused == false) {
					model = tree_view.get_model ();

					if (model != null) {
						if (model.get_iter_first (out iter) == false) {
							/* No records listed, nothing to play... */
							return;
						}

						selection = tree_view.get_selection ();
						if (selection.count_selected_rows () == 0) {
							model.get (iter, Column.URI, out uri);

							selection.select_iter (iter);
							now_playing_iter = iter;
						} else {
							selection.get_selected (null, out iter);
							model.get (iter, Column.URI, out uri);
							now_playing_iter = iter;
						}
					}
				}

				Global.player.start_playing (uri);
			} else {
				Global.player.pause ();
			}
		}

		[CCode (instance_pos = -1)]
		public void on_next_action_cb (Gtk.Action source) {
			TreeModel model = null;
			TreeSelection selection = null;
			string uri = null;
			TreeIter iter;

			if (unlikely (Global.state != State.READY)) {
				return;
			}

			model = tree_view.get_model ();
			selection = tree_view.get_selection ();

			if (unlikely (model == null || selection == null)) {
				return;
			}

			if (selection.count_selected_rows () != 0) {
				selection.get_selected (null, out iter);
				if (model.iter_next (ref iter) == true) {
					selection.select_iter (iter);

					if (Global.now_playing == true) {
						model.get (iter, Column.URI, out uri);

						Global.player.start_playing (uri);
						now_playing_iter = iter;
					}
				}
			}
		}

		[CCode (instance_pos = -1)]
		public void on_record_stop_action_cb (Gtk.Action source) {
			if (unlikely (Global.state != State.READY)) {
				return;
			}

			if (Global.now_recording == false) {
				status_label.set_text (_("Initializing a new record..."));
				Global.recorder.start_recording ();
			} else {
				Global.recorder.stop_recording ();
			}
		}

		[CCode (instance_pos = -1)]
		public void on_about_action_cb (Gtk.Action source) {
			show_about_dialog (this as Window,
			                   "logo-icon-name", Config.PACKAGE,
			                   "title", _("About ") + Config.PACKAGE_NAME,
			                   "version", Config.PACKAGE_VERSION,
			                   "comments", _("A simple sound recorder for the GNOME desktop"),
			                   "copyright", Global.copyrights,
			                   "authors", Global.authors);
		}

		[CCode (instance_pos = -1)]
		public bool on_button_press_even_cb (Scale source) {
			if (unlikely (Global.state != State.READY)) {
				return false;
			}

			Global.seeking = true;

			return false;
		}

		[CCode (instance_pos = -1)]
		public void on_value_changed_cb (Scale source) {
			double location = 0.0;
			int64 duration = 0;
			int64 position = 0;

			if (unlikely (Global.state != State.READY)) {
				return;
			}

			if (Global.seeking == true) {
				location = scale_adjustment.get_value ();
				duration = Global.player.query_duration ();

				if (duration >= 0) {
					position = (int64) ((double) duration * location);

					Global.player.move (location);
					position_label.set_text (Utils.duration_to_string (position));
				}
			}
		}

		[CCode (instance_pos = -1)]
		public bool on_button_release_even_cb (Scale source) {
			if (unlikely (Global.state != State.READY)) {
				return false;
			}

			Global.seeking = false;

			return false;
		}

		[CCode (instance_pos = -1)]
		public void on_volume_changed_cb (ScaleButton source, double volume) {
			if (unlikely (Global.state != State.READY)) {
				return;
			}

			Global.player.set_volume (volume);
		}

		[CCode (instance_pos = -1)]
		public void on_row_activated_cb (TreeView source, TreePath path, TreeViewColumn column) {
			TreeModel model = null;
			string uri = null;
			TreeIter iter;

			if (unlikely (Global.state != State.READY)) {
				return;
			}

			model = tree_view.get_model ();

			if (likely (model != null)) {
				model.get_iter (out iter, path);
				model.get (iter, Column.URI, out uri);

				Global.player.start_playing (uri);
				now_playing_iter = iter;
			}
		}

		[CCode (instance_pos = -1)]
		public void on_selection_changed_cb () {
			TreeModel model = null;
			TreeSelection selection = null;
			TreeIter iter, previous, next;

			if (unlikely (Global.state != State.READY)) {
				return;
			}

			model = tree_view.get_model ();
			selection = tree_view.get_selection ();

			if (unlikely (model == null || selection == null)) {
				return;
			}

			if (selection.count_selected_rows () == 0) {
				save_as_action.set_sensitive (false);
				delete_action.set_sensitive (false);
				properties_action.set_sensitive (false);

				previous_action.set_sensitive (false);
				next_action.set_sensitive (false);

				scale_adjustment.set_value (0.0);
				position_label.set_text ("0:00");
				duration_label.set_text ("0:00");
			} else {
				save_as_action.set_sensitive (true);
				delete_action.set_sensitive (true);
				properties_action.set_sensitive (true);
				properties_dialog.reset ();

				selection.get_selected (null, out iter);
				previous = iter;
				next = iter;

				if (model.iter_previous (ref previous) == false) {
					previous_action.set_sensitive (false);
				} else {
					previous_action.set_sensitive (true);
				}

				if (model.iter_next (ref next) == false) {
						next_action.set_sensitive (false);
				} else {
						next_action.set_sensitive (true);
				}
			}
		}
	}
}

