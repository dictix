#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include <gtk/gtk.h>

#include "libdictix/dix-spectrograph.h"

int
main (int argc, char* argv[])
{
/*	gfloat db = -80.0f;*/
/*	gfloat level = 0.0f;*/

/*	if (argc != 2) {*/
/*		return 1;*/
/*	}*/

/*	db = strtof (argv[1], NULL);*/
/*	printf ("%f\n", db);*/

/*	if (db < -70.0f) {*/
/*		level = 0.0f;*/
/*	} else if (db < -60.0f) {*/
/*		level = (db + 70.0f) * 0.25f;*/
/*	} else if (db < -50.0f) {*/
/*		level = (db + 60.0f) * 0.5f + 2.5f;*/
/*	} else if (db < -40.0f) {*/
/*		level = (db + 50.0f) * 0.75f + 7.5f;*/
/*	} else if (db < -30.0f) {*/
/*		level = (db + 40.0f) * 1.5f + 15.0f;*/
/*	} else if (db < -20.0f) {*/
/*		level = (db + 30.0f) * 2.0f + 30.0f;*/
/*	} else if (db < 6.0f) {*/
/*		level = (db + 20.0f) * 2.5f + 50.0f;*/
/*	} else {*/
/*		level = 115.0f;*/
/*	}*/

/*	printf ("%f\n", level);*/

	gfloat levels[] = {-70.0f, -40.0f, -30.0f, -15.0f, -20.0f, -30.0f, -40.0f, -40.0f};

	GtkWidget* window = NULL;
	GtkWidget* spectrograph = NULL;

	g_type_init ();
	gtk_init (&argc, &argv);

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	g_signal_connect (window, "destroy",
	                  G_CALLBACK (gtk_main_quit), NULL);

	spectrograph = dix_spectrograph_new ();
dix_spectrograph_set_magnitudes ((DixSpectrograph *) spectrograph, 8, levels);

	gtk_container_add (GTK_CONTAINER (window), spectrograph);
	gtk_widget_show_all (window);

	gtk_main ();

	return 0;
}
