/**
 * Dictix / recorder.vapi
 *
 * Copyright (C) Martin Blanchard 2011 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

using GLib;
using Gtk;
using Gst;

namespace Dix {
	[CCode (prefix = "Dix", lower_case_cprefix = "dix_", cheader_filename = "dix-player.h")]

	[Compact]
	[CCode (free_function = "gst_object_unref")]
	public class Player : Pipeline {
		[CCode (cname = "dix_player_new")]
		public Player ();

		[CCode (cname = "dix_player_start_playing")]
		public void start_playing (string uri = 0, Error *error = null);
		[CCode (cname = "dix_player_query_duration")]
		public int64 query_duration ();
		[CCode (cname = "dix_player_query_position")]
		public int64 query_position ();
		[CCode (cname = "dix_player_query_progress")]
		public double query_progress (out int64 position);
		[CCode (cname = "dix_player_move")]
		public void move (double location);
		[CCode (cname = "dix_player_pause")]
		public void pause ();
		[CCode (cname = "dix_player_stop")]
		public void stop ();

		[CCode (cname = "dix_player_set_volume")]
		public void set_volume (double volume);

		public signal void playback_started ();
		public signal void playback_stopped (bool eos);
	}

	[CCode (prefix = "Dix", lower_case_cprefix = "dix_", cheader_filename = "dix-recorder.h")]

	[Compact]
	[CCode (free_function = "gst_object_unref")]
	public class Recorder : Pipeline {
		[CCode (cname = "dix_recorder_new")]
		public Recorder (string directory);

		[CCode (cname = "dix_recorder_start_recording")]
		public void start_recording (Error *error = null);
		[CCode (cname = "dix_recorder_stop_recording")]
		public void stop_recording ();

		public signal void record_started ();
		public signal void record_stopped (string uri);
	}

	[CCode (prefix = "Dix", lower_case_cprefix = "dix_", cheader_filename = "dix-spectrograph.h")]

	[Compact]
	[CCode (free_function = "gtk_widget_destroy")]
	public class Spectrograph : Widget {
		[CCode (cname = "dix_spectrograph_new")]
		public Spectrograph ();

		[CCode (cname = "dix_spectrograph_set_magnitudes")]
		public void set_magnitudes (uint bands, float[] magnitudes);
	}

	[CCode (prefix = "Dix", lower_case_cprefix = "dix_", cheader_filename = "dix-tag-reader.h")]

	[Compact]
	[CCode (free_function = "gst_object_unref")]
	public class TagReader : Pipeline {
		[CCode (cname = "dix_tag_reader_new")]
		public TagReader (string uri);

		[CCode (cname = "dix_tag_reader_load")]
		public void load ();

		[CCode (cname = "dix_tag_reader_get_title")]
		public string get_title ();
		[CCode (cname = "dix_tag_reader_get_codec")]
		public string get_codec ();
		[CCode (cname = "dix_tag_reader_get_size")]
		public uint64 get_size ();
		[CCode (cname = "dix_tag_reader_get_duration")]
		public int64 get_duration ();
		[CCode (cname = "dix_tag_reader_get_channels")]
		public int get_channels ();
		[CCode (cname = "dix_tag_reader_get_sample_rate")]
		public int get_sample_rate ();
		[CCode (cname = "dix_tag_reader_get_bit_rate")]
		public int get_bit_rate ();
	}

	[CCode (prefix = "Dix", lower_case_cprefix = "dix_", cheader_filename = "dix-tag-writer.h")]

	[Compact]
	[CCode (free_function = "gst_object_unref")]
	public class TagWriter : Pipeline {
		[CCode (cname = "dix_tag_writer_new")]
		public TagWriter (string uri);

		[CCode (cname = "dix_tag_writer_save")]
		public void save ();

		[CCode (cname = "dix_tag_writer_set_title")]
		public void set_title (string title);
	}

	[CCode (prefix = "Dix", lower_case_cprefix = "dix_", cheader_filename = "dix-transcoder.h")]

	[Compact]
	[CCode (free_function = "gst_object_unref")]
	public class Transcoder {
		[CCode (cname = "dix_transcoder_new")]
		public Transcoder ();

		[CCode (cname = "dix_transcoder_pre_load")]
		public void pre_load (string uri);
		[CCode (cname = "dix_transcoder_transcode")]
		public void transcode (string uri);
	}
}

